package functions;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import enums.exportMode;
import enums.understoodType;
import objects.understood.UnderstoodCrop;
import objects.understood.UnderstoodInterface;
import objects.understood.UnderstoodLand;

/**
 * This class writes data to files.
 * 
 * @author James_Seibel
 * @version 06-04-2020
 */
public class Exporter 
{
	/** this is how the data will be separated in our files when exporting */
	public String delimiter;
	
	/** this is the extension our files will have on export, not including the period.
	 * For example "csv" or "tsv". */
	public String fileExtension;
	
	/**
	 * default constructor. Exports to a csv file.
	 */
	public Exporter()
	{
		//set the delimiter to csv by default
		delimiter = ",";
		fileExtension = "csv";
	}
	
	/**
	 * constructor that accepts the file delimiter (what separates the data)
	 * and the file extension (txt, csv, tsv, ext.).
	 * @param newDelimiter the file delimiter to use
	 * @param newFileExtension the file extension to use
	 * 
	 * @throws IllegalArgumentException if the newDelimiter is longer than 1 character
	 */
	public Exporter(String newDelimiter, String newFileExtension) throws IllegalArgumentException
	{
		if(newDelimiter.length() != 1)
		{
			// only allow a single character for the delimiter
			throw new IllegalArgumentException("Delimiter can only be one character long.");
		}
		
		//set the delimiter to csv by default
		delimiter = newDelimiter;
		fileExtension = newFileExtension;
	}
	
	/**
	 * This writes the given understood objects to a file at the given
	 * folderPath.
	 * 
	 * @param folderPathString this is where the data will be written to.
	 * 						Generally written in this form: "./Interpreted data/..."
	 * @param data This is the data to export.
	 * 				Where the first arraylist is the year and the second
	 * 				is the understood objects related to that year.
	 * 				NOTE: only give one type of understood object at a time,
	 * 				failure to do so will cause problems.
	 * @param dataType this is used to determine what type of understood objects
	 * 					are being exported.
	 * @param mode this is used to determine how the files should be exported.
	 * 				Specifically whether a combined file and/or the
	 * 				individual files should be written
	 */
	public void writeDataToFile(String folderPathString,UnderstoodInterface[][] data, understoodType dataType, exportMode mode)
	{
		// this is the name of the file we are going to write
		String fileName = "";
		
		// this is what we will be using to write our files
		BufferedWriter bw = null;
		
		
		// try to write our data to a file
		try
		{
			// check if the folder we want to export to exists
			File folderPath = new File(folderPathString);
			
			// does the path exist?
			if(!folderPath.exists())
				// it doesn't, create the path
				folderPath.mkdirs();
			
			
			// write the combined file, if requested
			if(mode == exportMode.COMPLETE || mode == exportMode.ONLY_COMBINATION)
			{
				String name, filePath;
				switch(dataType)
				{
				case CROP:
					name = "_All_Crops";
					break;
				case LAND:
					name = "_All_Land";
					break;
				default:
					System.err.println("ERROR: unkown datatype \"" + dataType + "\", stoping Export.");
					return;
				}
				fileName = name + "_interpreted." + fileExtension;
				
				// this is where the new file will be created
				filePath = folderPath +"/" + fileName;
				
				// this is where we will write to
				File outputFile = new File(filePath);
				
				// check if the file exists, if not, create it
				makeSureFileExists(outputFile);
				
				bw = new BufferedWriter(new FileWriter(outputFile));
				
				// crop
				if(dataType == understoodType.CROP)
				{
					writeCropHeader(bw);
					// go through every year
					for(int year = 0; year < data.length; year++)
						// go through every crop
						for(UnderstoodInterface u : data[year])
							if(u != null)
								writeCropLine(bw, (UnderstoodCrop) u);
				}
				
				// field
				if(dataType == understoodType.LAND)
				{
					writeLandHeader(bw);
					// go through every year
					for(int year = 0; year < data.length; year++)
						// go through every field
						for(UnderstoodInterface u : data[year])
							if(u != null)
								writeLandLine(bw, (UnderstoodLand) u);
				}
				
				
				closeBufferedWriter(bw);
				
				System.out.println(fileName +" writing completed.\n");
			}
			
			
			
			// write the individual files, if requested
			if(mode == exportMode.COMPLETE || mode == exportMode.ONLY_INDIVIDUAL)
			{
				String filePath = "";
				
				// find the valid fields or crops
				ArrayList<String> exportNames = null;
				switch(dataType)
				{
				case CROP:
					exportNames = getValidCrops(data);
					break;
				case LAND:
					exportNames = getValidFields(data);
					break;
				default:
					System.err.println("ERROR: unkown datatype: \"" + dataType + "\", stoping export.");
					return;
				}
				
				// write a file for each exportName
				for(String name : exportNames)
				{
					fileName = name + "_interpreted." + fileExtension;
					
					// this is where the new file will be created
					filePath = folderPath +"/" + fileName;
					
					// this is where we will write to
					File outputFile = new File(filePath);
					
					// check if the file exists, if not, create it
					makeSureFileExists(outputFile);
					
					bw = new BufferedWriter(new FileWriter(outputFile));
					
					switch(dataType)
					{
					case LAND:
						writeLandHeader(bw);
						break;
					case CROP:
						writeCropHeader(bw);
						break;
					}
					
					// go through each year
					for(int year = 0; year < data.length; year++)
					{
						// only write data if there is data to write
						if(data[year].length != 0)
							// look through every understoodObject...
							for(int landIndex = 0; landIndex < data[year].length; landIndex++)
							{
								// ... and only write data related to this file
								switch(dataType)
								{
								case LAND:
									if(data[year][landIndex] != null &&
									  ((UnderstoodLand) data[year][landIndex]).name.equalsIgnoreCase(name))
										writeLandLine(bw, (UnderstoodLand) data[year][landIndex]);
									break;
								case CROP:
									if(data[year][landIndex] != null &&
									  ((UnderstoodCrop) data[year][landIndex]).name.equalsIgnoreCase(name))
										writeCropLine(bw, (UnderstoodCrop) data[year][landIndex]);
									break;
								}
							}
					}// end of year loop
					
					closeBufferedWriter(bw);
					System.out.println(fileName + ": sucessfully written.");
				}// end of exportName loop
				
				
				
			}// end of individual file export
			
		}// end of writing file
		catch(IOException e)
		{
			System.err.println("Error: " + fileName + " writing failed!");
			e.printStackTrace();
		}
		
		closeBufferedWriter(bw);
		
	}

	/**
	 * This checks to make sure that a given file exists,
	 * and if it doesn't, create it. 
	 * @param outputFile the file to test/create
	 * @throws IOException if the file is unable to be created
	 */
	private void makeSureFileExists(File outputFile) throws IOException
	{
		if(!outputFile.exists())
		{
			outputFile.createNewFile();
			System.out.println(outputFile.getPath() + ": not found, creating...");
		}
		else
		{
			System.out.println(outputFile.getPath() + ": found, overwriting...");
		}
	}

	/**
	 * A safe way to close a buffered writer.
	 * It will handle if the bw is null or throws an exception.
	 * @param bw the buffered writer to close
	 */
	private void closeBufferedWriter(BufferedWriter bw)
	{
		try
		{
			if(bw!=null)
				bw.close();
		}
		catch(IOException e)
		{
			System.err.println("Error: couldn't close BufferedWriter.");
			e.printStackTrace();
		}
	}

	/**
	 * This method writes the header for a crop file
	 * @param bw the BufferedWriter that will be used
	 * @throws IOException If the writing fails
	 */
	private void writeCropHeader(BufferedWriter bw) throws IOException
	{
		// write the top row, so we can see what each column is
		bw.write("crop" + delimiter);
		bw.write("year" + delimiter);
		bw.write("total area (acres)" + delimiter);
		
		bw.write("total production (bu)" + delimiter);
		bw.write("average Yield (bu/ac)" + delimiter);
		bw.write("average GDUs (or GDDs)" + delimiter);
		
		bw.write("average Rain (in.)" + delimiter);
		bw.write("average moisture (%)" + delimiter);
		
		bw.write("planting Start Date" + delimiter);
		bw.write("planting End Date" + delimiter);

		bw.write("harvest Start Date" + delimiter);
		bw.write("harvest End Date");

		bw.write("\n");
	}
	
	/**
	 * This method writes the data from a single understood object as
	 * a single line to the given file.
	 * @param uCrop where we get the data from
	 * @param bw the BufferedWriter that will be used
	 * @param year the current year we exporting
	 * @param cropIndex the current crop we are exporting
	 * @throws IOException
	 */
	private void writeCropLine(BufferedWriter bw, UnderstoodCrop uCrop) throws IOException
	{
		bw.write(uCrop.name + delimiter);
		bw.write(uCrop.getYear() + delimiter);
		bw.write(uCrop.totalAreaAcres + delimiter);
		
		bw.write(uCrop.totalYield + delimiter);
		bw.write(uCrop.avgYield + delimiter);
		bw.write(uCrop.avgGDUs + delimiter);
		
		bw.write(uCrop.avgRain + delimiter);
		bw.write(uCrop.avgMoisture + delimiter);
		
		bw.write(uCrop.getPlantingStartDate().toString() + delimiter);

		bw.write(uCrop.getPlantingEndDate().toString() + delimiter);

		bw.write(uCrop.getHarvestStartDate().toString() + delimiter);

		bw.write(uCrop.getHarvestEndDate().toString());

		bw.write("\n");
	}
	
	/**
	 * This writes the header for a land file
	 * @param bw the BufferedWriter we are using
	 * @throws IOException
	 */
	private void writeLandHeader(BufferedWriter bw) throws IOException 
	{
		bw.write("field" + delimiter);
		bw.write("crop" + delimiter);
		bw.write("year" + delimiter);
		bw.write("area (acre)" + delimiter);
		
		bw.write("total production (bu)" + delimiter);
		bw.write("Yield (bu/ac)" + delimiter);
		
		bw.write("Corn Yield (bu/ac)" + delimiter);
		bw.write("Soybean Yield (bu/ac)" + delimiter);
		bw.write("Wheat Yield (bu/ac)" + delimiter);
		
		bw.write("GDUs (or GDDs)" + delimiter);
		bw.write("Rain (in.)" + delimiter);
		bw.write("Moisture (%)" + delimiter);

		bw.write("planting Start Date" + delimiter);
		bw.write("planting End Date" + delimiter);

		bw.write("harvest Start Date" + delimiter);
		bw.write("harvest End Date");

		bw.write("\n");
	}
	
	/**
	 * This method writes the data from a single understood object as
	 * a single line to the given file.
	 * @param uLand the landResults that we are getting out data from
	 * @param bw the BufferedWriter that will be used
	 * @param year the current year we exporting
	 * @param landIndex the current piece of land we are exporting
	 * @throws IOException
	 */
	private void writeLandLine(BufferedWriter bw, UnderstoodLand uLand) throws IOException
	{
		bw.write(uLand.name + delimiter);
		bw.write(uLand.crop + delimiter);
		bw.write(uLand.getYear() + delimiter);

		bw.write(uLand.areaAcres + delimiter);
		bw.write(uLand.totalYield + delimiter);
		bw.write(uLand.avgYield + delimiter);
		
		
		// write the specific yield for the 3 main crops
		if(uLand.crop.equalsIgnoreCase("CORN"))
		{
			bw.write(uLand.avgYield + delimiter);
			bw.write("" + delimiter);
			bw.write("" + delimiter);
		}
		else if(uLand.crop.equalsIgnoreCase("SOYBEANS"))
		{
			bw.write("" + delimiter);
			bw.write(uLand.avgYield + delimiter);
			bw.write("" + delimiter);
		}
		else if(uLand.crop.equalsIgnoreCase("WHEAT"))
		{
			bw.write("" + delimiter);
			bw.write("" + delimiter);
			bw.write(uLand.avgYield + delimiter);
		}
		else
		{
			bw.write("" + delimiter);
			bw.write("" + delimiter);
			bw.write("" + delimiter);
		}
		
		
		bw.write(uLand.GDUs + delimiter);
		
		bw.write(uLand.rain + delimiter);
		bw.write(uLand.avgMoisture + delimiter);
		
		bw.write(uLand.getPlantingStartDate().toString() + delimiter);

		bw.write(uLand.getPlantingEndDate().toString() + delimiter);

		bw.write(uLand.getHarvestStartDate().toString() + delimiter);

		bw.write(uLand.getHarvestEndDate().toString());

		bw.write("\n");
	}
	
	/**
	 * This returns all valid crops within the given understood objects.
	 * NOTE: this will return null if it isn't given an array of understoodCrop objects
	 * @param cropResults a double arraylist of understoodCrops
	 * @param land an arrayList containing the names of every valid crop
	 */
	private ArrayList<String> getValidCrops(UnderstoodInterface[][] cropResults)
	{
		// this is what we are going to return
		ArrayList<String> land = new ArrayList<>();
		
		// go through every year
		for(UnderstoodInterface[] uYear : cropResults)
			// go through each piece of land
			for(UnderstoodInterface u : uYear)
			{
				if(u == null)
					// some years won't have any files in them
					// just skip to the next year
					continue;
				
				try
				{
					UnderstoodCrop uC = (UnderstoodCrop) u;
					// only add new items that aren't empty
					// and that we don't already have
					if(!land.contains(uC.name) && !uC.name.equals(""))
						land.add(uC.name);
				}
				catch(ClassCastException e)
				{
					System.err.println("ERROR: casting Understood to UnderstoodCrop failed.");
					e.printStackTrace();
				}
			}
		
		return land;
	}
	
	/**
	 * This returns all valid fields within the given understood objects.
	 * NOTE: this will return null if it isn't given an array of understoodLand objects
	 * @param landResults a double arraylist of understoodLands
	 * @param land an arrayList containing the names of every valid crop
	 */
	private ArrayList<String> getValidFields(UnderstoodInterface[][] landResults)
	{
		// this is what we are going to return
		ArrayList<String> land = new ArrayList<>();
		
		// go through every year
		for(UnderstoodInterface[] uYear : landResults)
			// go through each piece of land
			for(UnderstoodInterface u : uYear)
			{
				if(u == null)
					// some years won't have any files in them
					// just skip to the next year
					continue;
				
				try
				{
					UnderstoodLand uL = (UnderstoodLand) u;
					// only add new items that aren't empty
					// and that we don't already have
					if(!land.contains(uL.name) && !uL.name.equals(""))
						land.add(uL.name);
				}
				catch(ClassCastException e)
				{
					System.err.println("ERROR: casting Understood to UnderstoodCrop failed.");
					e.printStackTrace();
				}
			}
		
		return land;
	}
	
}

