package helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import enums.crop;

/**
 * This object holds the aliases for crops and can be used to determine
 * what crop a specific variety is.
 * 
 * @author James_Seibel
 * @version 3-31-2020
 */
public class CropAliases
{
	/**
	 * This array holds the different crop aliases
	 */
	private ArrayList<ArrayList<String>> cropAliases;
	
	/**
	 * constructor
	 * reads the file "CropNames.csv" from the given directory.
	 * NOTE: If for some reason there are two lines
	 * in that file with the same header (i.e. there are two places where "corn" 
	 * is the first word in the list) then all names from the first line
	 * will be overwritten by the second.
	 * @param cropNameDirectory
	 * @throws IOException file read error
	 * @throws Exception file format invalid or file not found
	 */
	public CropAliases(String cropNameDirectory) throws IOException
	{
		///////////////////////////////
		// create required variables //
		///////////////////////////////
		
		// create the 2D array crop array
		// by default it holds corn, soybeans, wheat, sunflowers, and barley
		// in that order
		cropAliases = new ArrayList<ArrayList<String>>();
		cropAliases.add(new ArrayList<String>()); // corn
		cropAliases.add(new ArrayList<String>()); // soybeans
		cropAliases.add(new ArrayList<String>()); // wheat
		cropAliases.add(new ArrayList<String>()); // sunflowers
		cropAliases.add(new ArrayList<String>()); // barley
		
		
		////////////////////////
		// read CropNames.csv //
		////////////////////////
		
		// check if there is a "CropNames.csv" file
		// and if there is use it when interpreting the field
		// (so similar crops can be grouped together)
		
		// create the reader, and try to find the file
		BufferedReader in = new BufferedReader(new FileReader(new File(cropNameDirectory)));
		
		// for each of the 5 known crops
		// search for aliases in the file
		for(int i = 0; i < 5; i++)
		{
			// read in the line
			String line = in.readLine();
			
			// get the index for each item in the line
			int index[] = findIndexOfItems(line);
			
			// get what names this line holds
			// IE: corn or wheat
			String cropName = line.substring(0,index[0]);
			
			readCropNameLine(line, index, cropName);
		}
		
		// close the reader
		in.close();
	}
	
	
	/**
	 * Read the given line, determine what crop it is holding, and
	 * then add them to the crops list.
	 * @param line the string we are reading
	 * @param commaIndexes the index of each comma in the line
	 * @param name the name of the crop this line holds
	 */
	private void readCropNameLine(String line, int[] commaIndexes, String name)
	{
		// add each name to the corresponding array
		switch(name)
		{
		case "corn":
			addItemsToCropAliases(line, commaIndexes, crop.CORN);
			break;
		case "soybeans":
			addItemsToCropAliases(line, commaIndexes, crop.SOYBEANS);
			break;
		case "wheat":
			addItemsToCropAliases(line, commaIndexes, crop.WHEAT);
			break;
		case "sunflowers":
			addItemsToCropAliases(line, commaIndexes, crop.SUNFLOWERS);
			break;
		case "barley":
			addItemsToCropAliases(line, commaIndexes, crop.BARLEY);
			break;
		}
	}
	
	/**
	 * Add each item in the line to the corresponding crop alias.
	 * @param line The string that contains every alias
	 * @param commaIndexes The index of every comma in the line
	 * @param cropIndex
	 */
	private void addItemsToCropAliases(String line, int[] commaIndexes, crop cropIndex)
	{
		String item;
		
		// go through and add each item in the line to the array
		for(int j = 0; j < commaIndexes.length-1; j++)
		{
			// find the next name in the list
			item = line.substring(commaIndexes[j]+1,commaIndexes[j+1]);
			
			// only add this name if it is actually a name, and not empty
			if(item != null && !item.equals(""))
				cropAliases.get(cropIndex.index).add(item);
		}
	}
	
	/**
	 * this finds the index of each comma in the given line.
	 * The returned array will also include the index of the final character in the line.
	 * @param line the string to find the indexes in
	 * @return an array that holds the location of each tab character
	 */
	private int[] findIndexOfItems(String line)
	{
		// this arraylist will hold the index of each tab as we find them
		ArrayList<Integer> indexes = new ArrayList<Integer>();

		// this variable is used to store the index of the tab we just found
		int nextItem = -1;

		// look through the line and find each tab character
		for(int i = 0; i < line.length(); i++)
		{
			// find the first tab character after i
			nextItem = line.indexOf(',', i);

			if(nextItem != -1)
			{
				// we haven't reached the end yet, 
				// add this tab to the arrayList and keep going
				i = nextItem;
				indexes.add(nextItem);
			}
			else
			{
				// we have reached the end,
				// add the end of the line to the index list and stop the loop
				indexes.add(line.length());
				i = line.length();
			}
		}

		// create an array so we won't have to return an arraylist
		int[] returnArray = new int[indexes.size()];

		// take all the data from the arrayList and place it in the array
		for(int i = 0; i < indexes.size(); i++)
		{
			returnArray[i] = indexes.get(i);
		}

		return returnArray;
	}
	
	/**
	 * this function returns what crop type the following variety is.
	 * Ex: AG008X7 would return Soybeans, and Spring Wheat would return wheat.
	 * If nothing exists, then we return the inputed variety.
	 * @param variety
	 * @return
	 */
	public String determineCropType(String variety)
	{
		// go through each item in the crops arraylist
		for(int i = 0; i < cropAliases.size(); i++)
		{
			for(int j = 0; j < cropAliases.get(i).size(); j++)
			{
				// the old logic, more strict
				//variety.equalsIgnoreCase(crops.get(i).get(j))
				if(variety.contains(cropAliases.get(i).get(j)))
				{
					// we found what crop this variety is
					// return the crop
					return cropAliases.get(i).get(0);
				}
			}
		}
		
		// we didn't find anything, just return the variety
		return variety;
	}
}
