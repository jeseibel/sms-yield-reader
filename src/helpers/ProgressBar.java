package helpers;

/**
 * This object prints out a loading bar.
 * 
 * @author James_Seibel
 * @version 06-08-2020
 */
public class ProgressBar
{
	/** this is the current progress of the loading bar */
	private double percent = 0;
	
	/**
	 * constructor
	 */
	public ProgressBar()
	{
		percent = 0;
	}
	
	/**
	 * constructor
	 * 
	 * @param newPercent the percentage to start at
	 */
	public ProgressBar(double newPercent)
	{
		percent = newPercent;
	}
	
	/**
	 * This prints out a percentage bar, this method should
	 * be called all the way through 100%,
	 * 
	 * @param newPercent the new percent to print, between 0 and 1
	 */
	public void printPercentage(double newPercent)
	{
		newPercent = newPercent * 100.0;
		
		// only continue if the new percent is greater than the old
		if(newPercent < percent)
			return;
		
		
		for(int i = (int)percent; i <= newPercent; i++)
		{
			if(i % 5 == 0)
				System.out.print(i);
			else
				System.out.print(".");
		}
		
		// print a new line char if the loading bar is done
		if(newPercent >= 100)
			System.out.println();
		
		// we add 1 so that we can't print the same percent again
		percent = newPercent + 1;
	}
}
