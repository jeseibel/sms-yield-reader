package helpers;

import java.io.File;
import java.util.ArrayList;

/**
 * This class holds static methods that may be helpful
 * in a variety of different applications, but don't require a
 * dedicated object to be used.
 * 
 * @author James_Seibel
 * @version 06-04-2020
 */
public class MiscHelpers
{
	/**
	 * This method takes in a string and returns the index of 
	 * the first character that matches charToFind
	 * after the startingIndex.
	 * 
	 * @param s the string to look through
	 * @param charToFind 
	 * @param startingIndex where to start looking for charToFind
	 * @return the index of the first charToFind in s
	 */
	public static int findNextChar(String s, char charToFind, int startingIndex)
	{
		s = s.substring(startingIndex, s.length());
		
		return s.indexOf(charToFind) + startingIndex;
	}	
	
	/**
	 * go through the given directory and add any files that have the
	 * correct file extension to a new arrayList
	 * 
	 * @param directory the directory to look through
	 * @param fileArray the arraylist to add the found files too
	 */
	public static ArrayList<File> filesToArrayList(File directory,String fileExtension)
	{
		ArrayList<File> fileArray = new ArrayList<>();
		
		// if there aren't any files here
		// don't try to add any
		if(directory.listFiles() == null)
			return null;
		
		// place every csv file in the directory into this array
		for(File f:directory.listFiles())
		{
			// check if this file has the extension we are looking for
			if(f.getName().substring(
					f.getName().length()-fileExtension.length(),
					f.getName().length()).equals(fileExtension))
			{
				// this file is valid, add it to the array
				fileArray.add(f);
			}
		}
		
		return fileArray;
	}
}
