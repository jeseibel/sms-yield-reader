package enums;

/**
 * This enum is used to differentiate understood objects.
 * @author James_Seibel
 * @version 4-1-2020
 */
public enum understoodType
{
	LAND (0),
	CROP (1);
	
	public final int index;
	
	understoodType(int newIndex)
	{
		index = newIndex;
	}
}

