package enums;

/**
 * 
 * @author James_Seibel
 * @version 04-05-2020
 */
public enum chemicalRateUnits
{
	UNKOWN (-1),
	LBS_ACRE (0),
	GAL_ACRE (1);
	
	public final int index;
	
	chemicalRateUnits(int newIndex)
	{
		index = newIndex;
	}
}
