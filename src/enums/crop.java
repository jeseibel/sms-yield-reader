package enums;


public enum crop
{
	CORN (0),
	SOYBEANS (1),
	WHEAT (2),
	SUNFLOWERS (3),
	BARLEY (4);
	
	public final int index;
	
	crop(int newIndex)
	{
		index = newIndex;
	}
}

