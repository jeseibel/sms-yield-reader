package enums;

/**
 * 
 * @author James_Seibel
 * @version 05-31-2020
 */
public enum smsFileType
{
	UNKOWN(-1),
	PLANTING (0),
	HARVEST (1),
	CHEMICAL(2);
	
	public final int index;
	
	smsFileType(int newIndex)
	{
		index = newIndex;
	}
}
