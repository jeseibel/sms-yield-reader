package enums;

/**
 * This enum is used to more easily determine how file exporting should happen.
 * @author James_Seibel
 * @version 4-1-2020
 */
public enum exportMode
{
	COMPLETE (0),
	ONLY_INDIVIDUAL (1),
	ONLY_COMBINATION (2);
	
	public final int index;
	
	exportMode(int newIndex)
	{
		index = newIndex;
	}
}

