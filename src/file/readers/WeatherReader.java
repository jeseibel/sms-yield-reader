package file.readers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import file.parsers.WeatherParser;
import helpers.MiscHelpers;
import helpers.ProgressBar;
import objects.Weather;
import objects.farm.FarmYear;

/**
 * Readers take in a directory and send the files over to
 * the corresesponding parser, which then interpret the
 * data stored in the files.
 * 
 * @author James_Seibel
 * @version 06-04-2020
 */
public class WeatherReader
{
	
	/**
	 * Read in all weather csv files in the given directory
	 * and add their data to the appropriate farmYear.
	 * @param weatherFileLocation the directory where the weather files are stored
	 * @return the number of correctly parsed files
	 * @throws FileNotFoundException if no files are found
	 */
	public static int readWeatherFiles(FarmYear[] farmYears, String weatherFileLocation) throws FileNotFoundException
	{		
		// open the folder containing the data files
		File directory = new File(weatherFileLocation);
		
		// this is where we will store every csv file that is
		// in the weatherFileLocation
		ArrayList<File> fileArray = MiscHelpers.filesToArrayList(directory,"csv");
		
		// only continue if there are files to read
		if(fileArray.isEmpty())
		{
			System.err.println("ERROR: NDAWN files expected in: \"" + directory.getAbsolutePath() + "\"");
			throw new FileNotFoundException();
		}
		
		// say how many files are in the folder
		System.out.println(fileArray.size() + " NDAWN files avaliable to parse");
		
		int parsedFiles = parseWeatherFiles(farmYears, fileArray);
		
		
		System.out.println(parsedFiles + " files successfully parsed");
		System.out.println();
		
		return parsedFiles;
	}
	
	/**
	 * parse the weather files and output the number of 
	 * files correctly parsed
	 * @param fileArray the files to try and parse 
	 * @return the number of files parsed
	 */
	private static int parseWeatherFiles(FarmYear[] farmYears, ArrayList<File> fileArray)
	{	
		// this will be used to determine the correct location of the newly parsed file
		int year = -1;
		
		int numParsedFiles = 0;
		
		ProgressBar loading = new ProgressBar();
		
		// parse each file in the list
		for(File f:fileArray)
		{
			loading.printPercentage(numParsedFiles/ (double)fileArray.size());
			
			// try to read the file
			try
			{
				// create a weather object from this file
				Weather weatherData = WeatherParser.createWeatherObjectFromFile(f);
				
				// find out the year for this field so we know where to put it in storage
				year = weatherData.year;
				
				// add the weather object to the corresponding year
				farmYears[year-2000].weatherData = weatherData;
				
				// we have successfully parsed one more file
				numParsedFiles++;
			}
			catch(IOException e)
			{
				// this file wasn't set up correctly,
				// ignore the thrown exception and move on to
				// the next file
				System.err.println("ERROR: \"" + f + "\" reading Failed, moving on to next file...");
			}
		}
		
		loading.printPercentage(1);
		
		return numParsedFiles;
	}
}
