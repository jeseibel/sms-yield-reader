package file.readers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import file.parsers.SMS_Parser;
import helpers.CropAliases;
import helpers.MiscHelpers;
import helpers.ProgressBar;
import objects.farm.FarmYear;
import objects.farm.Field;

/**
 * Readers take in a directory and send the files over to
 * the corresesponding parser, which then interpret the
 * data stored in the files.
 * 
 * @author James_Seibel
 * @version 06-04-2020
 */
public class SMS_Reader
{
	/** this is how many significant figures should be used
	 * for fields. */
	private static final int NUMBER_OF_SIG_FIGS = 3;
	
	/**
	 * Read in all SMS files in the given directory
	 * and add their data as field objects to the corresponding
	 * farmYear.
	 * 
	 * @param farmYears the farm years that will store the data from the files
	 * @param SMSFileLocation the directory where the SMS files are stored
	 * @param cropAliases used to determine crop names
	 * @param fileType the name of the files that are going to be parsed; 
	 * 					only used for printing to the console.
	 * 
	 * @return the number of correctly parsed files
	 * @throws FileNotFoundException if no files are found
	 */
	public static int readSmsDirectory(FarmYear[] farmYears, 
			String SMSFileLocation, CropAliases cropAliases, String fileType) throws FileNotFoundException
	{
		// open the folder containing the data files
		File directory = new File(SMSFileLocation);
		
		ArrayList<File> fileArray = MiscHelpers.filesToArrayList(directory,"txt");
		
		// only continue if there are files to read
		if(fileArray.isEmpty())
		{
			System.err.println("ERROR: " + fileType + " files expected in: \"" + directory.getAbsolutePath() + "\"");
			throw new FileNotFoundException();
		}
		
		// say how many files are in the folder
		System.out.println(fileArray.size() + " " + fileType + " files avaliable to parse");
		
		int numParsedFiles = parseFileList(farmYears, fileArray, cropAliases);
		
		
		System.out.println(numParsedFiles + " files successfully parsed");
		System.out.println();
		
		return numParsedFiles;
	}
	
	/**
	 * Parse all SMS files in the given directory
	 * and add their data as field objects to the corresponding
	 * farmYear.
	 * 
	 * @param farmYears the farm years that will store the fields
	 * @param fileArray the files to parse
	 * @param cropAliases the known list of crop aliases
	 * 
	 * @return the number of correctly parsed files
	 */
	private static int parseFileList(FarmYear[] farmYears, ArrayList<File> fileArray, CropAliases cropAliases)
	{
		// create the parser
		SMS_Parser parser = new SMS_Parser(cropAliases);
		
		// this will store the land we get from the file
		Field newLand;
		
		int year = -1; // this will be changed for each file we read
		int numParsedFiles = 0;
		
		ProgressBar loading = new ProgressBar();
		
		// parse each file in the list
		for(File f:fileArray)
		{
			loading.printPercentage(numParsedFiles/ (double)fileArray.size());
			
			// try to read the file
			// if the file isn't formatted correctly it will
			// throw an exception
			try
			{
				// parse the file, if the file isn't set up correctly
				// it will throw an exception and we will go on to
				// the next file in the array
				newLand = parser.createFieldObjectFromFile(f);
				
				
				year = newLand.year;

				// truncate the data in the newLand, to improve readability
				newLand.truncateData(NUMBER_OF_SIG_FIGS);

				// add the newly parsed land to the farm list at the appropriate year
				// but, only if the land has a name
				if(!newLand.name.equals(""))
					farmYears[year-2000].addOrMergeField(newLand);
				
				numParsedFiles++;
			}
			catch(Exception e)
			{
				System.err.println("ERROR: file \"" + f.getName() + "\" " + e.getMessage());
			}
		}
		
		loading.printPercentage(1);
		
		return numParsedFiles;
	}
}
