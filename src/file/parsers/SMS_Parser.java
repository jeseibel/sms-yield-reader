package file.parsers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import enums.chemicalRateUnits;
import enums.smsFileType;
import helpers.CropAliases;
import objects.Chemical;
import objects.SimpleDate;
import objects.farm.ChemicalApplication;
import objects.farm.Field;
import objects.parsing.PendingObject;

/**
 * Parsers take in files from a reader and then
 * turn the data stored there into interpreted objects.
 * 
 * @author James_Seibel
 * @version 06-04-2020
 */
public class SMS_Parser 
{
	/** This is the BufferedReader, so we can read through our file */
	private BufferedReader in;
	
	/** This is used to determine crop names */
	private CropAliases cropAliases;
	
	/** this is a temporary variable that store the line that is currently being read */
	private String line = "";

	
	/** this is the number of data lines that have been read */
	private int numberOfLines = 0;
	
	/** This determines what type of file this is */
	private smsFileType fileType = smsFileType.UNKOWN;
	
	/** This array holds the indexes of each column (and the end of the line) */
	private int[] columnIndexes;

	/** This array holds the name of each column */
	private String[] columnNames;
	
	
	
	
	
	
	
	
	
	/**
	 * constructor
	 * @param cropAliases the known list of crop aliases 
	 */
	public SMS_Parser(CropAliases newCropAliases)
	{
		cropAliases = newCropAliases;
		
		// set all variables to their defaults
		resetVariables();
	}
	
	
	
	
	
	
	
	/**
	 * create a single field object from a single SMS csv file.
	 * 
	 * @param newFile this is the file that will be parsed
	 * @param sigFigs the number of significant figures to use when truncating data.
	 * 				This prevents numbers from becoming unwieldy and hard to read
	 */
	public Field createFieldObjectFromFile(File newFile)
	{
		// reset all variables to their default state, so we can read this new file correctly
		resetVariables();
		
		PendingObject pending = new PendingObject();
		Field newLand = null;
		
		// start reading in the file
		try 
		{
			// create the reader
			in = new BufferedReader(new FileReader(newFile));

			// start reading the file
			try
			{
				// get the first line
				line = in.readLine();

				// find where the start and end of each column is
				columnIndexes = findIndexOfColumns(line);

				// Initialize the variable to store the name of each column
				columnNames = new String[columnIndexes.length - 1]; 
					// this is -1 since the last number in the columnIndexes 
					// variable is the last character in the line, and not the 
					// index of a column name

				// find and store the name of the first column (since it is a special case)
				columnNames[0] = line.substring(0, columnIndexes[0]);

				// find and store the name of every other column
				for(int i = 1; i < columnIndexes.length -1; i++)
					columnNames[i] = line.substring(columnIndexes[i] + 1, columnIndexes[i+1]);

				
				
				
				////////////////////
				// parse the file //
				////////////////////
				
				// read in the first line of the file
				// for anything that only needs to be read once
				// (ie the name or the crop)
				line = in.readLine();
				
				// update the columnIndexes
				updateColumnIndexes(line);
				
				
				// determine what type of file this is
				determineFileType(); // throws an exception if the file type isn't known
				
				// get all the one time data (name, product, and date)
				getOneTimeInfo(pending);
				
				// parse the rest of this line
				parseLine(pending);
				
				
				// read in the rest of the file
				// if this line is null then we have reached the end
				while((line = in.readLine()) != null)
				{
					// update the columnIndexes
					updateColumnIndexes(line);
					
					// parse the line
					parseLine(pending);
				}
				
				
				
				//////////////////////
				// save our results //
				//////////////////////
				
				newLand = createFieldFromPending(pending);
			}
			catch(IOException e)
			{
				System.err.println("ERROR: I/O Exception: " + newFile.getName() + " reading FAILED" + "\n" +
						   numberOfLines + " lines successfully read.");
			}
			catch(IllegalArgumentException e)
			{
				System.out.println("NOTE: file type not recognized: " + newFile.getName() + " skipped");
			}

			// we have finished reading through the file			
		}
		catch (FileNotFoundException e) 
		{
			System.err.println("ERROR: " + newFile.getName() + " not found.");
			e.printStackTrace();
			
			// don't export anything
			return null;
		}
		finally
		{
			// close the BufferedReader after everything is done
			try { in.close(); } 
			catch (IOException e) { e.printStackTrace(); }
		}
		
		////////////////////////
		// export our results //
		////////////////////////

		return newLand;
		
	} // end of parse File

		
	
	
	
	
	
	
	
	
	
	/**
	 * this finds the index of each column in the given line.
	 * This is done by finding the tab character.
	 * The returned array will also include the index of the final character in the line.
	 * @param line the string to find the indexes in
	 * @return an array that holds the location of each tab character
	 */
	private static int[] findIndexOfColumns(String line)
	{
		// this arraylist will hold the index of each tab as we find them
		ArrayList<Integer> indexes = new ArrayList<Integer>();

		// this variable is used to store the index of the tab we just found
		int nextTab = -1;

		// look through the line and find each tab character
		for(int i = 0; i < line.length(); i++)
		{
			// find the first tab character after i
			nextTab = line.indexOf('\t', i);

			if(nextTab != -1)
			{
				// we haven't reached the end yet, 
				// add this tab to the arrayList and keep going
				i = nextTab;
				indexes.add(nextTab);
			}
			else
			{
				// we have reached the end,
				// add the end of the line to the index list and stop the loop
				indexes.add(line.length());
				i = line.length();
			}
		}

		// create an array so we won't have to return an arraylist
		int[] returnArray = new int[indexes.size()];

		// take all the data from the arrayList and place it in the array
		for(int i = 0; i < indexes.size(); i++)
		{
			returnArray[i] = indexes.get(i);
		}

		return returnArray;
	}

	/**
	 * this is a wrapper function for findInexOfColumns that 
	 * inputs the result into columnIndexes, the only difference is the name.
	 * (so it is easier to read the code)
	 * @param line
	 */
	private void updateColumnIndexes(String line)
	{
		columnIndexes = findIndexOfColumns(line);
	}
	
	/**
	 * This function reads in whatever is currently in the "line"
	 * variable and interprets what needs to be added to our running totals
	 * @param pending the pendingField to add the data too
	 */
	private void parseLine(PendingObject pending)
	{
		// update the number of lines we have read through
		numberOfLines++;
		
		// set up the iterator
		int loop = 0;
		
		// is this a harvest file?
		if(fileType == smsFileType.HARVEST)
		{
			parseHarvestLine(pending);
		}
		// is this a planting file?
		else if(fileType == smsFileType.PLANTING)
		{
			// nothing special needs to be grabbed
		}
		else if(fileType == smsFileType.CHEMICAL)
		{
			parseChemicalLine(pending);
		}
		
		
		// refresh the iterator
		loop = 0;
		
		// find the total distance traveled
		while(!columnNames[loop].equals("Distance(ft)"))
			loop++;
		// try to add it to our running total
		try 
		{
			pending.distance += Double.parseDouble(line.substring(columnIndexes[loop]+1,columnIndexes[loop+1]));
		}
		catch(NumberFormatException e)
		{
			System.err.println("ERROR: distance reading failed");
			e.printStackTrace();
		}
		
	}// end of parse line
	
	/**
	 * 
	 * @param pending
	 */
	private void parseChemicalLine(PendingObject pending)
	{
		int loop = 0;
		
		// find the yield 
		while(!columnNames[loop].equals("Rt Apd Liq(gal(US)/ac)"))
			loop++;
		// try to add it to our running total
		try 
		{
			pending.pendingChemical += Double.parseDouble(line.substring(columnIndexes[loop]+1,columnIndexes[loop+1]));
		}
		catch(NumberFormatException e)
		{
			System.err.println("ERROR: pending chemical reading failed");
			e.printStackTrace();
		}
		// refresh the iterator
		loop = 0;
		
		
		// find the implement width
		while(!columnNames[loop].equals("Swth Wdth(ft)"))
			loop++;
		// try to add it to our running total
		try 
		{
			pending.pendingImplementWidth += Double.parseDouble(line.substring(columnIndexes[loop]+1,columnIndexes[loop+1]));
		}
		catch(NumberFormatException e)
		{
			System.err.println("ERROR: implement Width reading failed");
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param pending
	 */
	private void parseHarvestLine(PendingObject pending)
	{
		int loop = 0;
		// find the yield 
		while(!columnNames[loop].equals("Yld Vol(Dry)(bu/ac)"))
			loop++;
		// try to add it to our running total
		try 
		{
			pending.pendingYield += Double.parseDouble(line.substring(columnIndexes[loop]+1,columnIndexes[loop+1]));
		}
		catch(NumberFormatException e)
		{
			System.err.println("ERROR: pending yield reading failed");
			e.printStackTrace();
		}
		// refresh the iterator
		loop = 0;
		
		
		// find the implement width
		while(!columnNames[loop].equals("Swth Wdth(ft)"))
			loop++;
		// try to add it to our running total
		try 
		{
			pending.pendingImplementWidth += Double.parseDouble(line.substring(columnIndexes[loop]+1,columnIndexes[loop+1]));
		}
		catch(NumberFormatException e)
		{
			System.err.println("ERROR: implement Width reading failed");
			e.printStackTrace();
		}
		// refresh the iterator
		loop = 0;
		
		
		// find the harvest moisture
		while(!columnNames[loop].equals("Moisture(%)"))
			loop++;
		// try to add it to our running total
		try 
		{
			pending.pendingMoisture += Double.parseDouble(line.substring(columnIndexes[loop]+1,columnIndexes[loop+1]));
		}
		catch(NumberFormatException e)
		{
			System.err.println("ERROR: Moisture reading failed");
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Get info that doesn't change between lines, specifically:
	 * the field's name, product, and date.
	 * 
	 * @param pending the land we want to update
	 */
	private void getOneTimeInfo(PendingObject pending)
	{
		// set up an iterator for use when finding different pieces of data
		int loop = 0;
		
		// find the index of the field name
		while(!columnNames[loop].equals("Field"))
			loop++;
		// set the field's name
		pending.name = line.substring(columnIndexes[loop]+1,columnIndexes[loop+1]);
		
		// find the index of the crop or chemical name
		while(!columnNames[loop].equals("Product"))
			loop++;
		
		if(fileType == smsFileType.HARVEST || fileType == smsFileType.PLANTING)
			// get the crop or variety
			pending.crop = line.substring(columnIndexes[loop]+1,columnIndexes[loop+1]);
		else if(fileType == smsFileType.CHEMICAL)
			// the chemical name is also listed under "product"
			pending.chemical = line.substring(columnIndexes[loop]+1,columnIndexes[loop+1]);
		
		
		// refresh the iterator
		loop = 0;
		
		// find the index of the date
		while(!columnNames[loop].equals("Date"))
			loop++;
		// try to set the year
		try
		{
			// get the whole date
			// the data should be formated like this: mm/dd/yyyy
			// or like this: m/d/yyyy 
			// (since the month or day number can be in the single digits)
			String dateString = line.substring(columnIndexes[loop]+1,columnIndexes[loop+1]);
			
			// these indexes are used to find the date
			int secondIndex = dateString.lastIndexOf("/");
			int firstIndex = dateString.lastIndexOf("/", secondIndex -1);
			
			// find and set the date
			int month = Integer.parseInt(dateString.substring(0, firstIndex));
			int day = Integer.parseInt(dateString.substring(firstIndex+1, secondIndex));
			int year = Integer.parseInt(dateString.substring(secondIndex+1, dateString.length()));
			pending.date = new SimpleDate(month, day, year);
			
		}
		catch(Exception e)
		{
			System.err.println("ERROR: date reading failed");
		}
	}
	
	/**
	 * This function resets all variables to their default values.
	 * This helps prevent any data from spilling over from file to file,
	 * if this Parser reads multiple files.
	 */
	private void resetVariables()
	{
		line = "";
		numberOfLines = 0;
		columnIndexes = null;
		columnNames = null;
		
	}
	
	/**
	 * This function finds out what type of file is being read.
	 * @throws IllegalArgumentException if the given file is not recognized
	 */
	private void determineFileType() throws IllegalArgumentException
	{
		int loop = 0;
		// try to find the yield number in the file
		try 
		{
			while(!columnNames[loop].equals("Yld Vol(Dry)(bu/ac)"))
				loop++;
			
			// we found where the yield is, this file must be a harvest file
			fileType = smsFileType.HARVEST;
			return;
		}
		catch(Exception e)
		{
			// we reached the end of the columnNames array
			
			// do nothing, this just means that this file isn't a harvest file
		}
		
		
		
		// refresh the iterator
		loop = 0;
		// try to find the seed flow
		try 
		{
			while(!columnNames[loop].equals("SeedFlow(ksds/s)"))
				loop++;
			
			// we found where the seed flow is, this file must be a planting file
			fileType = smsFileType.PLANTING;
			return;
		}
		catch(Exception e)
		{
			// we reached the end of the columnNames array
			
			// do nothing, this just means that this file isn't a planting file
		}
		
		
		
		// refresh the iterator
		loop = 0;
		// try to find the boom pressure
		try 
		{
			while(!columnNames[loop].equals("Boom Pres(psi)"))
				loop++;
			
			// we found where the seed flow is, this file must be a planting file
			fileType = smsFileType.CHEMICAL;
			return;
		}
		catch(Exception e)
		{
			// we reached the end of the columnNames array
			
			// do nothing, this just means that this file isn't a chemical file
		}
		
		// we don't know what type of file this is
		fileType = smsFileType.UNKOWN;
		throw new IllegalArgumentException("File type not recognized.");
	}

	
	/**
	 * This function takes in a field and overwrites any previous data
	 * with whatever is current.
	 * @param land the land to update
	 * @param pending the pendingField which has the new data
	 * @return the land file we just updated
	 */
	private Field createFieldFromPending(PendingObject pending)
	{
		Field land = new Field();
		
		// get the average implement width
		double implementWidth = pending.pendingImplementWidth / numberOfLines;
		
		
		land.name = pending.name;
		land.year = pending.date.year;
		land.crop = cropAliases.determineCropType(pending.crop);
		
		
		// was this file a planting file?
		if(fileType == smsFileType.PLANTING)
		{
			// set the planting date
			land.plantingDate = pending.date;
			
			// we don't set the area since we only care about the area harvested
			// not the area planted
		}
		// or a harvest file?
		else if(fileType == smsFileType.HARVEST)
		{
			// only get the area of the field from harvest
			
			// find the area of the field in feet
			double newArea = implementWidth * pending.distance;

			// set the area in ft
			land.areaFt += newArea;

			// since we have the area in ft it is an easy conversion to Acres
			newArea = newArea / 43560;

			// set the area in acres
			land.areaAcres += newArea;
			
			
			// get the average moisture
			land.avgMoisture = pending.pendingMoisture / numberOfLines;
			
			// get the average yield
			land.avgYield = pending.pendingYield / numberOfLines;
			
			// set the harvest date
			land.harvestDate = pending.date;
		}
		else if(fileType == smsFileType.CHEMICAL)
		{
			Chemical chem = new Chemical();
			chem.name = pending.chemical;
			
			
			double newArea = implementWidth * pending.distance;
			// convert from ft to acres
			newArea = newArea / 43560;
			
			chem.amount = pending.pendingChemical / numberOfLines * newArea;
			chem.rateUnits = chemicalRateUnits.GAL_ACRE;
			
			ChemicalApplication chemApp = new ChemicalApplication();
			chemApp.chemicals.add(chem);
			chemApp.applicationDate = pending.date;
			land.chemicalApplications.add(chemApp);
		}
		
		
		
		return land;
	}
	
	
	
	
}// end of class
