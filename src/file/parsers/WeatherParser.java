package file.parsers;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import objects.Weather;

/**
 * Parsers take in files from a reader and then
 * turn the data stored there into interpreted objects.
 * 
 * @author James_Seibel
 * @version 06-04-2020
 */
public class WeatherParser 
{	
	/**
	 * Create a weather object for 1 year from a single csv file.
	 * 
	 * @param file the file that has the required weather information
	 * @return a completed weather object
	 * @throws IOException if the file isn't found
	 */
	public static Weather createWeatherObjectFromFile(File file) throws IOException
	{
		Weather w = new Weather();
		
		parseWeatherFile(file,w);
		
		return w;
	}
	
	/**
	 * This function reads a NDAWN csv file and places the data into
	 * the given weather object.
	 * @param newFile the name of the csv file to read from
	 * @param w the weather object
	 * @throws Exception if the file is not set up correctly
	 */
	private static void parseWeatherFile(File newFile, Weather w) throws IOException
	{
		// create all required variables
		
		BufferedReader in = null;
		
		String line;
		
		int[] columnIndexes;
		String[] columnNames;
		
		// these arrays should cover the whole year
		// even though we will only use part of each
		w.rain = new double[12][32];
		w.tempHigh = new double[12][32];
		w.tempLow = new double[12][32];
		
		// try to start reading the file
		try 
		{
			// create the reader
			in = new BufferedReader(new FileReader(newFile));
			
			// this is used to determine what line
			// had an error, if the reading fails
			int lineNumb = 1;
			
			// start reading the file
			try
			{
				// the first 3 lines don't have any info on them
				// so we need to go down to the 4th
				in.readLine(); lineNumb++;
				in.readLine(); lineNumb++;
				in.readLine(); lineNumb++;
				line = in.readLine(); lineNumb++;// this line actually has data

				// find where the start and end of each column is
				columnIndexes = getIndexOfColumns(line);

				// Initialize the variable to store the name of each column
				columnNames = new String[columnIndexes.length - 1]; 
				// this is minus 1 since the last number in the columnIndexes 
				// variable is the last character in the line, and not the 
				// index of a column name
				
				// find and store the name of the first column (since it is a special case)
				columnNames[0] = line.substring(0, columnIndexes[0]);
				
				// find and store the name of every other column
				for(int i = 1; i < columnIndexes.length -1; i++)
					columnNames[i] = line.substring(columnIndexes[i] + 1, columnIndexes[i+1]);
				
				
				
				///////////////////
				// Read the file //
				///////////////////
				
				// skip a empty line
				// (this line is an extension of the header line, and
				// doesn't have any data we need)
				in.readLine(); lineNumb++;
				
				// read in the first line of the file,
				// for anything that only needs to be read once
				// (in this case the year)
				line = in.readLine(); lineNumb++;

				// update the columnIndexes
				columnIndexes = getIndexOfColumns(line);
				
				int loop = 0;
				
				// find the year
				while(!columnNames[loop].equals("Year"))
					loop++;
				try 
				{ w.year = Integer.parseInt(line.substring(columnIndexes[loop]+1,columnIndexes[loop+1])); }
				catch(NumberFormatException e)
				{ System.out.println(e); }
				

				// parse the rest of this line
				parseLine(columnNames, columnIndexes, line, w);


				// read in the rest of the file
				// if this line is null then we have reached the end
				while((line = in.readLine()) != null)
				{
					lineNumb++;
					
					// update the columnIndexes
					columnIndexes = getIndexOfColumns(line);
					
					// parse the line
					parseLine(columnNames, columnIndexes, line, w);
				}
				
			}
			catch(IOException e)
			{
				// if there is a problem reading the file
				// stop reading and print out what file failed
				System.err.println("ERROR: file  \"" + newFile.getName() + "\" failed to read at line: " + lineNumb + "\n" + e);
			}
			
			// we have finished reading through the file
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			// close the BufferedReader after everything is done
			try { in.close(); } 
			catch (IOException e) { e.printStackTrace(); }
		}
	}
	
	
	/**
	 * This function reads in whatever is currently in the "line"
	 * and then writes the relevant information to the weather object.
	 * @param columnNames 
	 * @param columnIndexes  
	 * @param line 
	 * @param w 
	 * @throws IOException The file was formatted incorrectly
	 */
	private static void parseLine(String columnNames[], int columnIndexes[] , String line, Weather w) throws IOException
	{
		// day
		// month
		
		// rainfall
		// max temp
		// min temp
		
		// these variables are used to determine where to place each
		// item found in the file
		int day = -1;
		int month = -1;
		
		// set up the iterator
		int loop = 0;

		// refresh the iterator
		loop = 0;

		// find the day 
		while(!columnNames[loop].equals("Day"))
			loop++;
		// set the day
		try 
		{ 
			day = Integer.parseInt(line.substring(columnIndexes[loop]+1,columnIndexes[loop+1])); 
		}
		catch(NumberFormatException e)
		{ 
			throw new IOException("Day not found, file format incorrect.");
		}



		// refresh the iterator
		loop = 0;

		// find the month 
		while(!columnNames[loop].equals("Month"))
			loop++;
		// set the month
		try 
		{ 
			month = Integer.parseInt(line.substring(columnIndexes[loop]+1,columnIndexes[loop+1]));
		}
		catch(NumberFormatException e)
		{ 
			throw new IOException("Month not found, file format incorrect.");
		}


		// refresh the iterator
		loop = 0;

		// find the rainfall
		while(!columnNames[loop].equals("Rainfall"))
			loop++;
		// try to add it to the array
		try 
		{ 
			w.rain[month][day] = Double.parseDouble(line.substring(columnIndexes[loop]+1,columnIndexes[loop+1])); 
		}
		catch(NumberFormatException e)
		{ 
			/* do nothing
			 * usually it just means that there wasn't any number
			 * (ie " ")
			 */
		}



		// refresh the iterator
		loop = 0;

		// find the Max Temp
		while(!columnNames[loop].equals("Max Temp"))
			loop++;
		// try to add it to the array
		try 
		{
			w.tempHigh[month][day] = Double.parseDouble(line.substring(columnIndexes[loop]+1,columnIndexes[loop+1])); 
		}
		catch(NumberFormatException e)
		{ /* do nothing */ }
		
		
		
		// refresh the iterator
		loop = 0;

		// find the Min Temp
		while(!columnNames[loop].equals("Min Temp"))
			loop++;
		// try to add it to the array
		try 
		{ 
			w.tempLow[month][day] = Double.parseDouble(line.substring(columnIndexes[loop]+1,columnIndexes[loop+1]));
		}
		catch(NumberFormatException e)
		{ /* do nothing */ }
		
		
	}// end of line parsing
	
	
	
	/**
	 * this finds the index of each column in the given line.
	 * This is done by finding the tab character.
	 * The returned array will also include the index of the final character in the line.
	 * @param line the string to find the indexes in
	 * @return an array that holds the location of each tab character
	 */
	private static int[] getIndexOfColumns(String line)
	{
		// this arraylist will hold the index of each tab as we find them
		ArrayList<Integer> indexes = new ArrayList<Integer>();
		
		// this variable is used to store the index of the tab we just found
		int nextTab = -1;

		// look through the line and find each tab character
		for(int i = 0; i < line.length(); i++)
		{
			// find the first tab character after i
			nextTab = line.indexOf(',', i);

			if(nextTab != -1)
			{
				// we haven't reached the end yet, 
				// add this tab to the arrayList and keep going
				i = nextTab;
				indexes.add(nextTab);
			}
			else
			{
				// we have reached the end,
				// add the end of the line to the index list and stop the loop
				indexes.add(line.length());
				i = line.length();
			}
		}

		// create an array so we won't have to return an arraylist
		int[] returnArray = new int[indexes.size()];

		// take all the data from the arrayList and place it in the array
		for(int i = 0; i < indexes.size(); i++)
		{
			returnArray[i] = indexes.get(i);
		}

		return returnArray;
	}
}
