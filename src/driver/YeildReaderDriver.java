package driver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import helpers.MiscHelpers;

/** 
 * This is used to read the config file and pass relevant
 * information over to the main program so it can
 * run correctly.
 * 
 * @author James_Seibel
 * @version 04-05-2020
 */
public class YeildReaderDriver
{	
	public static void main(String[] args)
	{
		File config = new File("./config.txt");
		
		// if the config file doesn't exist, create it
		// and then stop the program.
		if(!config.exists())
		{
			System.out.println("Config file missing, creating...");
			try
			{
				config.createNewFile();
				
				FileWriter fw = new FileWriter(config);
				
				writeConfigFile(fw);
				
				fw.close();
				
				System.out.println("Config file sucessfully created.");
				System.out.println("go to \"" + config.getPath() + "\" and enter the required information.");
				return;
			}
			catch(IOException e)
			{
				System.err.println("ERROR: config file writing failed.");
				e.printStackTrace();
				System.out.println("\nShutting down program.");
				return;
			}
		}
		
		
		Program program = new Program();
		
		try
		{
			String weatherDir="", smsDir="", cropNamesFile="", chemicalDirectory="", s, temp;
			
			// try and read in the config file
			FileReader fr = new FileReader(config);
			BufferedReader br = new BufferedReader(fr);
			
			System.out.println("Reading config file");
			
			// read each line of the config file
			while((s = br.readLine()) != null)
			{
				int index = MiscHelpers.findNextChar(s, ' ', 0);
				if(index == -1)
					continue;
				
				// Interpret the line
				temp = s.substring(0,index);
				switch(temp)
				{
				case "weatherDirectory":
					System.out.println("found weather folder");
					weatherDir = s.substring(index,s.length()).trim();
					break;
				case "SMSDirectory":
					System.out.println("found SMS folder");
					smsDir = s.substring(index,s.length()).trim();;
					break;
				case "cropNamesFile":
					System.out.println("found crop name file");
					cropNamesFile = s.substring(index,s.length()).trim();
					break;
				case "SMSchemicalDirectory":
					System.out.println("found chemcial directory folder");
					chemicalDirectory = s.substring(index,s.length()).trim();
					break;
				default:
					break;
				}
			}
			br.close();
			
			// if there are any missing files don't run the program
			boolean errored = checkForMissingDir(weatherDir, smsDir, chemicalDirectory, cropNamesFile);
			if(errored)
				return;
			
			// if there are any duplicate files, don't run the program
			errored = checkForDuplicateDirs(weatherDir, smsDir, chemicalDirectory, cropNamesFile);
			if(errored)
				return;
			
			System.out.println("Config file sucessfully read.\n");
			
			
			
			// run the actual program
			program.run(weatherDir, smsDir, chemicalDirectory, cropNamesFile, "./Interpreted data/crops","./Interpreted data/land");
			
			
		}
		catch(FileNotFoundException e)
		{
			System.err.println("ERROR: file not found!");
			e.printStackTrace();
		}
		catch(IOException e)
		{
			System.err.println("ERROR: file reading failed!");
			e.printStackTrace();
		}
	}
	
	/**
	 * Return true if any of the directories are the same.
	 * Prints out an error message if any directories are the same.
	 * @param weatherDir
	 * @param smsDir
	 * @param chemicalDir
	 * @param cropNamesFile
	 * @return
	 */
	private static boolean checkForDuplicateDirs(String weatherDir, String smsDir, String chemicalDir, String cropNamesFile)
	{
		String directoryList[] = {weatherDir, smsDir, chemicalDir, cropNamesFile};
		
		// compare each string against each other
		for(int i = 0; i < directoryList.length; i++)
		{
			for(int j = i + 1; j < directoryList.length; j++)
			{
				if(directoryList[i].equals(directoryList[j]))
				{
					System.err.println("ERROR: there are multiple directories with the same name!");
					System.out.println("Check the config file, and make sure that each folder");
					System.out.println("you listed is different.");
					
					return true;
				}
			}
		}
		
		
		return false;
	}
	
	/**
	 * Return true if any of the directories weren't given.
	 * Prints out an error message for each blank string.
	 * @param weatherDir
	 * @param smsDir
	 * @param chemicalDirectory
	 * @param cropNamesFile
	 * @return
	 */
	private static boolean checkForMissingDir(String weatherDir, String smsDir, String chemicalDirectory, String cropNamesFile)
	{
		boolean missing = false;
		
		if(weatherDir.isEmpty())
		{
			System.err.println("ERROR: weather directory missing.");
			missing = true;
		}
		if(smsDir.isEmpty())
		{
			System.err.println("ERROR: weather directory missing.");
			missing = true;
		}
		if(chemicalDirectory.isEmpty())
		{
			System.err.println("ERROR: chemical directory missing.");
			missing = true;
		}
		if(cropNamesFile.isEmpty())
		{
			System.err.println("ERROR: weather directory missing.");
			missing = true;
		}
		return missing;
	}
	
	/**
	 * This creates the default config file
	 * @param fw the file writer to use
	 * @throws IOException if the file writer runs into trouble
	 */
	private static void writeConfigFile(FileWriter fw) throws IOException
	{
		fw.write("// NOTE: each folder must be different, otherwise\n");
		fw.write("//       the program won't run.\n\n");
		
		
		fw.write("// this folder is where the NDAWN files are located.\n");
		fw.write("weatherDirectory \n");
		fw.write("// this folder is where the SMS files are located.\n");
		fw.write("SMSDirectory \n");
		fw.write("// this folder is where the crop Names file is located.\n");
		fw.write("cropNamesFile \n");
		fw.write("// this folder is where the SMS chemical files are located.\n");
		fw.write("SMSchemicalDirectory \n\n");
		
		fw.write("// Example of how to write folders:\n");
		fw.write("// weatherDirectory C:/data/weather folder");
	}
}
