package driver;

import java.io.IOException;
import java.util.ArrayList;

import enums.exportMode;
import enums.understoodType;
import file.readers.SMS_Reader;
import file.readers.WeatherReader;
import functions.Exporter;
import helpers.CropAliases;
import interpreters.YearInterpreter;
import objects.SimpleDate;
import objects.farm.FarmYear;
import objects.farm.Field;
import objects.understood.UnderstoodCrop;
import objects.understood.UnderstoodLand;

/**
 * This is the base of the program, it holds all relevant data
 * and interprets it.
 * 
 * @author James_Seibel
 * @version 06-04-2020
 */
public class Program
{
	/** This variable holds every year the farm has been running;
	 * with 0 being the year 2000 (and the max being MAXNUMBYEARS)*/
	private FarmYear farmYears[];
	
	/** this is where we store the results of our data interpretation.
	 * Where the first arrayList is the year and the second are
	 *  the crops planted that year. */
	private UnderstoodCrop[][] cropResults;
	
	/** this is where we store the results of our data interpretation
	 * Where the first arrayList is the year and the second are
	 *  the fields planted that year. */
	private UnderstoodLand landResults[][];
	
	/** this object can return what crop a specific variety is */
	private CropAliases cropAliases;
	
	/** 
	 * this is the maximum number of years that this program can store,
	 *  starting at the year 2000
	 */
	private static final int MAXNUMBYEARS = 100;
	
	/**
	 * constructor
	 */
	public Program()
	{
		// set up farmList so we can store data up to the year 2000 + MAXNUMBYEARS
		farmYears = new FarmYear[MAXNUMBYEARS];

		// Initialize each year in the farmList
		for(int i = 0; i < MAXNUMBYEARS; i++)
		{
			farmYears[i] = new FarmYear(i + 2000);
		}
	}
	
	/**
	 * Read in data from the given folders and export the interpreted
	 * data 
	 * 
	 * @param weatherDirectory the folder that contains the NDAWN csv files
	 * @param smsDirectory the folder that holds the planting and harvest SMS files
	 * @param chemicalDirectory the folder that holds the chemical SMS files
	 * @param cropNamesFile the location of the file that contains the crop names
	 * @param cropExportDirectory the folder to write the crop data to
	 * @param landExportDirectory the folder to write the field data to
	 * @throws IOException If any of the files/folders don't exist
	 */
	public void run(String weatherDirectory, String smsDirectory, 
			String chemicalDirectory, String cropNamesFile,
			String cropExportDirectory, String landExportDirectory) throws IOException
	{
		cropAliases = new CropAliases(cropNamesFile); // throws an IO Exception
		
		
		
		System.out.println("=================================");
		System.out.println("     Starting File Parsing");
		System.out.println("=================================");
		System.out.println();
		
		// read in the weather files
		WeatherReader.readWeatherFiles(farmYears, weatherDirectory); // throws file not found exception
		// read in the SMS files
		SMS_Reader.readSmsDirectory(farmYears, smsDirectory, cropAliases, "SMS Planting and Harvest"); // throws file not found exception
		// read in the chemical SMS files
		SMS_Reader.readSmsDirectory(farmYears, chemicalDirectory, cropAliases, "SMS Chemical"); // throws file not found exception
		
		
		
		System.out.println("=========================================");
		System.out.println("  adding weather information to fields");
		System.out.println("=========================================");
		System.out.println();
		
		int numWeatherYears = addWeatherDataToFarmYearArray(farmYears);
		System.out.println(numWeatherYears + " years of weather information successfully added");
		System.out.println();
		
		
		
		
		System.out.println("=================================");
		System.out.println("  Starting data interpretation");
		System.out.println("=================================");
		System.out.println();
		
		interpretCropData(farmYears);	
		
		interpretFieldData(farmYears);
		
		
		
		System.out.println("=================================");
		System.out.println("        Starting export");
		System.out.println("=================================");
		System.out.println();
		
		exportData(",", "csv", cropExportDirectory, landExportDirectory);
		
		
		
		System.out.println("PROGRAM FINISHED");
		System.out.println("SHUTTING DOWN");
	}
	
	
	

	
	
	/**
	 * Add weather data to all farm years.
	 * 
	 * @return how many years data was successfully added
	 */
	private int addWeatherDataToFarmYearArray(FarmYear[] farmYears)
	{
		// how many years data has been successfully added
		int numOfYears = 0;
		
		// go through each year
		for(FarmYear fy:farmYears)
		{
			if(fy.getNumbFields() == 0)
			{
				// there aren't any fields this year, ignore it
				continue;
			}
			
			// make sure that there is weather data for this year
			if(fy.weatherData == null)
			{
				// this year doesn't have any weather data,
				// skip this year after printing an error
				System.err.println("ERROR: weather data missing for year " + fy.year);
				continue;
			}
			
			// go through each field
			for(int i = 0; i < fy.getNumbFields(); i++)
			{
				// get the field we want to add weather too
				Field field = fy.getField(i);
				
				
				
				// does this field have a planting date?
				if(field.plantingDate.day == -1)
				{
					field.plantingDate = new SimpleDate(4,15,-1);
					// keep the year at -1 to show that this is an estimate
				}
				
				// does this field have a harvest date?
				if(field.plantingDate.day == -1)
				{
					field.plantingDate = new SimpleDate(10,15,-1);
					// keep the year at -1 to show that this is an estimate
				}
				
				
				// go through each month and day, between the harvest and planting days
				// and add the corresponding data to the field
				for(int m = 0; m < fy.weatherData.rain.length; m++)
				{
					for(int d = 0; d < fy.weatherData.rain[m].length; d++)
					{
						// skip ahead
						if(m < field.plantingDate.month)
							m = field.plantingDate.month;
						
						// only add weather data between the planting and harvest dates
						
						// normal case
						if(m > field.plantingDate.month && m < field.plantingDate.month)
						{
							field.totalRain += fy.weatherData.rain[m][d];
							
						}
						// special case (first month)
						else if(m == field.plantingDate.month)
						{
							if(d >= field.plantingDate.day)
							{
								field.totalRain += fy.weatherData.rain[m][d];
							}
						}
						// special case (last month)
						else if(m == field.plantingDate.month)
						{
							if(d <= field.plantingDate.day)
							{
								field.totalRain += fy.weatherData.rain[m][d];
							}
						}
						
						
					} // weather day loop
				} // weather month loop
			} // field Loop
			
			numOfYears++;
		} // year loop
		
		return numOfYears;
	}
	
	/**
	 * Create understoodLand objects for each field.
	 * 
	 * @param farmYears the farmYears to look through
	 */
	private void interpretFieldData(FarmYear[] farmYears)
	{
		System.out.println("starting field interpretation");		
		
		// refresh the iterator
		int loop = 0;

		// try to start interpreting our data for the land
		try 
		{
			// temporary arraylist, since we don't know how big
			// the data set may be
			ArrayList<ArrayList<UnderstoodLand>> results = new ArrayList<>();
			
			// go through every year that we have data for
			for(FarmYear fy:farmYears)
			{
				// create a new arrayList to store our findings
				results.add(new ArrayList<UnderstoodLand>());

				// only continue if there is data for this year
				if(fy.getNumbFields() != 0)
				{
					// this is where we temporarily store the data we are interpreting, before putting
					// it into the results 2D arrayList
					ArrayList<UnderstoodLand> newResults = new ArrayList<UnderstoodLand>();

					newResults = YearInterpreter.interpreteYearForLand(fy);
					
					// go through each understood object and truncate the
					// decimals for easier reading
					for(UnderstoodLand u : newResults)
					{
						u.truncateData(3);
					}
					
					// create a new arraylist where we will put our newly found results
					results.add(loop,newResults);
				}

				loop++;

			} // end of data interpretation loop
			
			// allocate the memory needed for the array
			int maxSize = -1;
			for(ArrayList<UnderstoodLand> uA : results)
					if(uA.size() > maxSize)
						maxSize = uA.size();
			landResults = new UnderstoodLand[results.size()][maxSize];
			
			// convert the results to a static array
			int year = 0;
			int field = 0;
			for(ArrayList<UnderstoodLand> uA : results)
			{
				for(UnderstoodLand u : uA)
				{
					landResults[year][field] = u;
					field++;
				}
				field = 0;
				year++;
			}
		} 
		catch (Exception e) 
		{
			// something bad happened
			e.printStackTrace();
		}
		
		System.out.println("field interpretation finished");
		System.out.println();
	}
	
	/**
	 * Create understoodCrop objects for each crop.
	 * 
	 * @param farmYears the farmYears to look through
	 */
	private void interpretCropData(FarmYear[] farmYears)
	{
		System.out.println("starting crop interpretation");
		
		int loop = 0;
		
		// temporary arraylist, since we don't know how big
		// the data set may be
		ArrayList<ArrayList<UnderstoodCrop>> results = new ArrayList<>();
		
		try
		{
			// go through every year that we have data for
			for(FarmYear fy:farmYears)
			{
				// create a new arrayList to store our findings
				results.add(new ArrayList<UnderstoodCrop>());				
				
				// only continue if there is data for this year
				if(fy.getNumbFields() != 0)
				{					
					// this is where we temporarily store the data we are interpreting, before putting
					// it into the results 2D arrayList
					ArrayList<UnderstoodCrop> newResults = new ArrayList<UnderstoodCrop>();
					
					newResults = YearInterpreter.interpreteYearForCrops(fy);
					
					// go through each understood object and truncate the
					// decimals for easier reading
					for(UnderstoodCrop u : newResults)
						u.truncateData(3);
					
					// create a new arraylist where we will put our newly found results
					results.add(loop,newResults);
				}
				
				loop++;
				
			} // end of data interpretation loop
			
			// allocate the memory needed for the array
			int maxSize = -1;
			for(ArrayList<UnderstoodCrop> uA : results)
				if(uA.size() > maxSize)
					maxSize = uA.size();
			cropResults = new UnderstoodCrop[results.size()][maxSize];
			
			// convert the results to a static array
			int year = 0;
			int crop = 0;
			for(ArrayList<UnderstoodCrop> uA : results)
			{
				
				for(UnderstoodCrop u : uA)
				{
					cropResults[year][crop] = u;
					crop++;
				}
				crop = 0;
				year++;
			}
		}
		catch (Exception e) 
		{
			// something bad happened
			e.printStackTrace();
		}
		
		System.out.println("crop interpretation finished");
		System.out.println();
	}

	/**
	 * Write all relevant data to the given folder locations.
	 * 
	 * @param delimiter the character used to separate data in each file
	 * @param fileExtension the extension for each file
	 * @param cropExportFolder where to write the crop files to
	 * @param landExportFolder where to write the land files to
	 */
	private void exportData(String delimiter, String fileExtension, 
			String cropExportFolder, String landExportFolder)
	{
		// catch if an invalid delimiter was passed in
		try
		{
			// create the exporter
			Exporter dataWriter = new Exporter(delimiter,fileExtension);
			
			// export our crop data (the averages over all fields for a given year)
			System.out.println("starting crop export");
			dataWriter.writeDataToFile(cropExportFolder,cropResults,understoodType.CROP,exportMode.COMPLETE);
			System.out.println();
			System.out.println();
			
			// export all of our land data to one file
			System.out.println("starting land export");
			dataWriter.writeDataToFile(landExportFolder,landResults,understoodType.LAND,exportMode.COMPLETE);
			System.out.println();
			
			System.out.println();
			System.out.println("data export completed");
			System.out.println();
			
		}
		catch(IllegalArgumentException e)
		{
			System.err.println("ERROR: the delimiter " + delimiter + " is longer than 1 character.");
			System.err.println("data export skipped");
		}
	}
	
}
