package interpreters;


import java.util.ArrayList;

import objects.farm.FarmYear;
import objects.understood.UnderstoodCrop;

/**
 * This class is in charge of creating UnderstoodCrop objects.
 * @author James_Seibel
 * @version 06/03/2020
 *
 */
public class CropInterpreter 
{	
	/**
	 * Take in a farm year and output an arrayList containing an Understood
	 * object for each crop and variety
	 * @param fy the farm year to interpret
	 * @return an arrayList of understood objects
	 * @throws Exception if the same field had two different crops the same year
	 */
	public static ArrayList<UnderstoodCrop> interpreteYear(FarmYear fy)
	{
		// create an arraylist of Understood objects (one for each crop)
		ArrayList<UnderstoodCrop> results = new ArrayList<UnderstoodCrop>();
		
		// this is the name of the crop we are dealing with
		String cropName;
		
		// does a understood object exist for this crop?
		boolean cropExists = false;
		
		// go through every piece of field in this year
		for(int i = 0; i < fy.getNumbFields(); i++)
		{
			// we don't know if the required Understood objects exist yet
			cropExists = false;
			
			// read in the field and
			// check the crop name
			cropName = fy.getField(i).crop;
			
			// create a new Understood object if none exist
			if(results.size() == 0)
			{
				// no Understood objects existed, create one for the crop
				results.add(new UnderstoodCrop());
				results.get(0).name = cropName;
				
				cropExists = true;
			}
			else
			{
				// there is at least 1 understood object
				
				// see if there is an Understood object for this crop
				for(UnderstoodCrop u:results)
				{
					if(u.name.equalsIgnoreCase(cropName))
					{
						// we found one break out of the loop
						cropExists = true;
						break;
					}
				}			
			}
			
			// did we find an Understood object for this crop?
			if(cropExists)
			{
				// we did, do nothing
			}
			else
			{
				// we didn't, add a new Understood object
				
				results.add(new UnderstoodCrop());
				results.get(results.size()-1).name = cropName;
			}
			
			// find which Understood object we need to interact with
			// so we can start adding data
			for(UnderstoodCrop u:results)
			{
				// check if this Understood object is for the same crop
				if(u.name.equalsIgnoreCase(cropName))
				{
					// we found the correct understood object
					// add relative information
					
					// does it have an area?
					if(fy.getField(i).areaAcres != 0)
					{
						// this field has an area, continue
						
						// add the area from this field
						u.totalAreaAcres += fy.getField(i).areaAcres;
						u.totalAreaFt += fy.getField(i).areaFt;

						if(fy.year == 2016)
							System.out.println(u.name + " " + u.totalAreaAcres);
						
						// find the rain for this field and add it to the running average
						// but only say that we found another piece of land if the rain is not zero
						if(fy.getField(i).totalRain != 0)
						{
							// we multiply by the area so we can get the
							// weighted average later
							u.avgRain += fy.getField(i).totalRain * fy.getField(i).areaAcres;
						}

						// find the yield for this field and add it to the running average
						// but only say that we found another piece of land if there is yield data
						if(fy.getField(i).avgYield != 0)
						{
							// we multiply by the area so we can get the
							// weighted average later
							u.avgYield += fy.getField(i).avgYield * fy.getField(i).areaAcres;
						}

						// find the GDUs for this field and add them to the running average
						// but only say that we found another piece of land if the GDU's is not zero
						if(GDU.getGDUs(
								fy.getField(i),
								fy.weatherData) != 0)
						{
							// we multiply by the area so we can get the
							// weighted average later
							u.avgGDUs += GDU.getGDUs(
									fy.getField(i),
									fy.weatherData) * fy.getField(i).areaAcres;
						}
					}
					
					//
					// update the harvest and planting dates
					//
					
					// planting start
					if((u.getPlantingStartDate().day > fy.getField(i).plantingDate.day && 
							u.getPlantingStartDate().month >= fy.getField(i).plantingDate.month) 
							|| u.getPlantingStartDate().month > fy.getField(i).plantingDate.month)
					{
						u.getPlantingStartDate().day = fy.getField(i).plantingDate.day;
						u.getPlantingStartDate().month = fy.getField(i).plantingDate.month;
					}
					
					// planting end
					if((u.getPlantingEndDate().day < fy.getField(i).plantingDate.day && 
							u.getPlantingEndDate().month <= fy.getField(i).plantingDate.month) 
							|| u.getPlantingEndDate().month < fy.getField(i).plantingDate.month)
					{
						u.getPlantingEndDate().day = fy.getField(i).plantingDate.day;
						u.getPlantingEndDate().month = fy.getField(i).plantingDate.month;
					}
					
					// harvest start
					if((u.getHarvestStartDate().day > fy.getField(i).harvestDate.day && 
							u.getHarvestStartDate().month >= fy.getField(i).harvestDate.month) 
							|| u.getHarvestStartDate().month > fy.getField(i).harvestDate.month)
					{
						u.getHarvestStartDate().day = fy.getField(i).harvestDate.day;
						u.getHarvestStartDate().month = fy.getField(i).harvestDate.month;
					}
					
					// harvest end
					if((u.getHarvestEndDate().day < fy.getField(i).harvestDate.day && 
							u.getHarvestEndDate().month <= fy.getField(i).harvestDate.month) 
							|| u.getHarvestEndDate().month < fy.getField(i).harvestDate.month)
					{
						u.getHarvestEndDate().day = fy.getField(i).harvestDate.day;
						u.getHarvestEndDate().month = fy.getField(i).harvestDate.month;
					}
					// end of adding data to this understood object
				}
				// wrong Understood object, keep looking
			}
		}
				
		// set any one time variables (like the year)
		// and get the weighted averages for the relevant variables
		for(UnderstoodCrop u: results)
		{
			// set the year
			u.setYear(fy.year);
			
			// set the weighted averages
			u.avgGDUs /= u.totalAreaAcres;
			u.avgRain /= u.totalAreaAcres;
			u.avgYield /= u.totalAreaAcres;
		}
		
		return results;
	}
	
	
		
	
}
