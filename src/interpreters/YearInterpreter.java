package interpreters;

import java.util.ArrayList;

import objects.farm.FarmYear;
import objects.understood.UnderstoodCrop;
import objects.understood.UnderstoodLand;

/**
 * This object reads in farmYears and outputs
 * understood land and crop objects.
 * 
 * @author James_Seibel
 * @version 06/04/2020
 */
public class YearInterpreter 
{	
	/**
	 * Take in a farm year and output an arrayList containing an Understood
	 * object for each crop and variety
	 * @param fy the farm year to interpret
	 * @return an arrayList of understood objects
	 * @throws Exception if the same field had two different crops the same year
	 */
	public static ArrayList<UnderstoodLand> interpreteYearForLand(FarmYear fy) throws Exception
	{
		// create an arraylist of Understood objects (one for each crop)
		ArrayList<UnderstoodLand> results = new ArrayList<UnderstoodLand>();
		
		// this is the name of the land we are dealing with
		String landName;
		
		// does a understood object exist for this land?
		boolean landExists = false;
		
		// go through every piece of field in this year
		for(int i = 0; i < fy.getNumbFields(); i++)
		{
			// we don't know if the required Understood objects exist yet
			landExists = false;
			
			// set the name of the land
			landName = fy.getField(i).name;

			// create a new Understood object if none exist
			if(results.size() == 0)
			{
				// no Understood object existed, create one for this field
				results.add(new UnderstoodLand());
				results.get(0).name = landName;
			}
			
			// see if there is an Understood object for this field
			for(UnderstoodLand u:results)
			{
				if(u.name.equalsIgnoreCase(landName))
				{
					// we found one break out of the loop
					landExists = true;
					break;
				}
			}			
			
			
			// did we find an Understood object for this field?
			if(landExists)
			{
				// we did, do nothing
			}
			else
			{
				// we didn't, add a new Understood object
				
				results.add(new UnderstoodLand());
				results.get(results.size()-1).name = landName;
			}
			
			
			
			// find which Understood object we needs to interacted with
			// so we can start adding data
			for(UnderstoodLand u:results)
			{
				// check if this Understood object is for the same piece of land
				if(u.name.equalsIgnoreCase(landName))
				{
					// we found the correct understood object
					// add relative information
					
					// set the name of the crop grown
					// if we try to overwrite a previous crop name, throw an exception
					// (since a piece of land shouldn't have more than one crop grown at a time)
					if(u.crop != null)
					{
						u.crop = fy.getField(i).crop;
					}
					else
					{
						throw new Exception(fy.getField(i).name + " held more than one crop in " + fy.year);
					}
					
					// find the moisture for this field and add it to the running average
					if(fy.getField(i).avgMoisture != 0)
					{
						// we multiply by the area so we can get the
						// weighted average later
						u.avgMoisture += fy.getField(i).avgMoisture;
					}
					
					// find the rain for this field and add it to the running average
					if(fy.getField(i).totalRain != 0)
					{
						u.rain += fy.getField(i).totalRain;
					}
					
					// find the yield for this field and add it to the running average
					if(fy.getField(i).avgYield != 0)
					{
						u.avgYield += fy.getField(i).avgYield;
					}
					
					// find the GDUs for this field and add them to the running average
					if(GDU.getGDUs(fy.getField(i),fy.weatherData) != 0)
					{
						u.GDUs += GDU.getGDUs(fy.getField(i),fy.weatherData);
					}
					
					// set the year
					u.setYear(fy.year);
					
					// set the area of the field (in feet)
					u.areaFt = fy.getField(i).areaFt;
					
					// convert feet to acres
					u.areaAcres = fy.getField(i).areaFt / 43560;
					
					// using the area get the total yield for this field
					u.totalYield = u.areaAcres * u.avgYield;
					
					
					// update the harvest and planting dates
					
					// planting start
					if((u.getPlantingStartDate().day > fy.getField(i).plantingDate.day && 
							u.getPlantingStartDate().month >= fy.getField(i).plantingDate.month) 
							|| u.getPlantingStartDate().month > fy.getField(i).plantingDate.month)
					{
						u.getPlantingStartDate().day = fy.getField(i).plantingDate.day;
						u.getPlantingStartDate().month = fy.getField(i).plantingDate.month;
					}
					
					// planting end
					if((u.getPlantingEndDate().day < fy.getField(i).plantingDate.day && 
							u.getPlantingEndDate().month <= fy.getField(i).plantingDate.month) 
							|| u.getPlantingEndDate().month < fy.getField(i).plantingDate.month)
					{
						u.getPlantingEndDate().day = fy.getField(i).plantingDate.day;
						u.getPlantingEndDate().month = fy.getField(i).plantingDate.month;
					}
					
					// harvest start
					if((u.getHarvestStartDate().day > fy.getField(i).harvestDate.day && 
							u.getHarvestStartDate().month >= fy.getField(i).harvestDate.month) 
							|| u.getHarvestStartDate().month > fy.getField(i).harvestDate.month)
					{
						u.getHarvestStartDate().day = fy.getField(i).harvestDate.day;
						u.getHarvestStartDate().month = fy.getField(i).harvestDate.month;
					}
					
					// harvest end
					if((u.getHarvestEndDate().day < fy.getField(i).harvestDate.day && 
							u.getHarvestEndDate().month <= fy.getField(i).harvestDate.month) 
							|| u.getHarvestEndDate().month < fy.getField(i).harvestDate.month)
					{
						u.getHarvestEndDate().day = fy.getField(i).harvestDate.day;
						u.getHarvestEndDate().month = fy.getField(i).harvestDate.month;
					}
					
				}
				// wrong Understood object, keep looking
			}
		}
		
		// unlike the CropInterpreter we don't have to average anything.
		// Since each piece of field should only have 1 object 
		// in the farmYear anyway
		
		return results;
	}
	
	
	
	/**
	 * Take in a farm year and output an arrayList containing an Understood
	 * object for each crop and variety
	 * @param fy the farm year to interpret
	 * @return an arrayList of understood objects
	 * @throws Exception if the same field had two different crops the same year
	 */
	public static ArrayList<UnderstoodCrop> interpreteYearForCrops(FarmYear fy)
	{
		// create an arraylist of Understood objects (one for each crop)
		ArrayList<UnderstoodCrop> results = new ArrayList<UnderstoodCrop>();
		
		// this is the name of the crop we are dealing with
		String cropName;
		
		// does a understood object exist for this crop?
		boolean cropExists = false;
		
		// go through every piece of field in this year
		for(int i = 0; i < fy.getNumbFields(); i++)
		{
			// we don't know if the required Understood objects exist yet
			cropExists = false;
			
			// read in the field and
			// check the crop name
			cropName = fy.getField(i).crop;
			
			// create a new Understood object if none exist
			if(results.size() == 0)
			{
				// no Understood objects existed, create one for the crop
				results.add(new UnderstoodCrop());
				results.get(0).name = cropName;
				
				cropExists = true;
			}
			else
			{
				// there is at least 1 understood object
				
				// see if there is an Understood object for this crop
				for(UnderstoodCrop u:results)
				{
					if(u.name.equalsIgnoreCase(cropName))
					{
						// we found one break out of the loop
						cropExists = true;
						break;
					}
				}			
			}
			
			// did we find an Understood object for this crop?
			if(cropExists)
			{
				// we did, do nothing
			}
			else
			{
				// we didn't, add a new Understood object
				
				results.add(new UnderstoodCrop());
				results.get(results.size()-1).name = cropName;
			}
			
			// find which Understood object we need to interact with
			// so we can start adding data
			for(UnderstoodCrop u:results)
			{
				// check if this Understood object is for the same crop
				if(u.name.equalsIgnoreCase(cropName))
				{
					// we found the correct understood object
					// add relative information
					
					// does it have an area?
					if(fy.getField(i).areaAcres != 0)
					{
						// this field has an area, continue
						
						// add the area from this field
						u.totalAreaAcres += fy.getField(i).areaAcres;
						u.totalAreaFt += fy.getField(i).areaFt;
						
						// find the moisture for this field and add it to the running average
						if(fy.getField(i).avgMoisture != 0)
						{
							// we multiply by the area so we can get the
							// weighted average later
							u.avgMoisture += fy.getField(i).avgMoisture * fy.getField(i).areaAcres;
						}
						
						// find the rain for this field and add it to the running average
						if(fy.getField(i).totalRain != 0)
						{
							// we multiply by the area so we can get the
							// weighted average later
							u.avgRain += fy.getField(i).totalRain * fy.getField(i).areaAcres;
						}

						// find the yield for this field and add it to the running average
						if(fy.getField(i).avgYield != 0)
						{
							// we multiply by the area so we can get the
							// weighted average later
							u.avgYield += fy.getField(i).avgYield * fy.getField(i).areaAcres;
						}
						
						// find the GDUs for this field and add them to the running average
						if(GDU.getGDUs(fy.getField(i),fy.weatherData) != 0)
						{
							// we multiply by the area so we can get the
							// weighted average later
							u.avgGDUs += GDU.getGDUs(fy.getField(i),fy.weatherData) * fy.getField(i).areaAcres;
						}
					}
					
					//
					// update the harvest and planting dates
					//
					
					// planting start
					if((u.getPlantingStartDate().day > fy.getField(i).plantingDate.day && 
							u.getPlantingStartDate().month >= fy.getField(i).plantingDate.month) 
							|| u.getPlantingStartDate().month > fy.getField(i).plantingDate.month)
					{
						u.getPlantingStartDate().day = fy.getField(i).plantingDate.day;
						u.getPlantingStartDate().month = fy.getField(i).plantingDate.month;
					}
					
					// planting end
					if((u.getPlantingEndDate().day < fy.getField(i).plantingDate.day && 
							u.getPlantingEndDate().month <= fy.getField(i).plantingDate.month) 
							|| u.getPlantingEndDate().month < fy.getField(i).plantingDate.month)
					{
						u.getPlantingEndDate().day = fy.getField(i).plantingDate.day;
						u.getPlantingEndDate().month = fy.getField(i).plantingDate.month;
					}
					
					// harvest start
					if((u.getHarvestStartDate().day > fy.getField(i).harvestDate.day && 
							u.getHarvestStartDate().month >= fy.getField(i).harvestDate.month) 
							|| u.getHarvestStartDate().month > fy.getField(i).harvestDate.month)
					{
						u.getHarvestStartDate().day = fy.getField(i).harvestDate.day;
						u.getHarvestStartDate().month = fy.getField(i).harvestDate.month;
					}
					
					// harvest end
					if((u.getHarvestEndDate().day < fy.getField(i).harvestDate.day && 
							u.getHarvestEndDate().month <= fy.getField(i).harvestDate.month) 
							|| u.getHarvestEndDate().month < fy.getField(i).harvestDate.month)
					{
						u.getHarvestEndDate().day = fy.getField(i).harvestDate.day;
						u.getHarvestEndDate().month = fy.getField(i).harvestDate.month;
					}
					// end of adding data to this understood object
				}
				// wrong Understood object, keep looking
			}
		}
				
		// set any one time variables (like the year)
		// and get the weighted averages for the relevant variables
		for(UnderstoodCrop u: results)
		{
			// set the year
			u.setYear(fy.year);
			
			// set the weighted averages
			u.avgGDUs /= u.totalAreaAcres;
			u.avgRain /= u.totalAreaAcres;
			u.avgYield /= u.totalAreaAcres;
			u.avgMoisture /= u.totalAreaAcres;
			
			// get the total production of this crop
			u.totalYield = u.avgYield * u.totalAreaAcres;
		}
		
		return results;
	}
	
	
	
	
}
