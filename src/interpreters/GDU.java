package interpreters;

import objects.Weather;
import objects.farm.Field;

/**
 * This class is able to determine how many GDUs (Growth Development Units)
 * a specific field experienced over the year.
 * 
 * @author James_Seibel
 * @version 06-03-2020
 */
public class GDU
{
	/**
	 * @param crop
	 * @param weatherData
	 * @return the GDUs between the planting date and harvest date
	 */
	public static int getGDUs(Field field, Weather weatherData)
	{
		int GDUs = 0;
		double maxTemp = 0;
		double minTemp = 0;
		
		
		// is this crop wheat?
		if(field.crop.equalsIgnoreCase("wheat"))
		{
			// use wheat's GDD function
			
			// go through every day and add up the GDUs
			for(int m = 0; m < weatherData.tempHigh.length; m++)
			{
				for(int d = 0; d < weatherData.tempHigh[m].length; d++)
				{
					// skip ahead if we can
					if(m < field.plantingDate.month)
						m = field.plantingDate.month;
					
					
					// get the highs and lows for this day
					maxTemp = weatherData.tempHigh[m][d];
					minTemp = weatherData.tempLow[m][d];
					
					// only add GDU data between the planting and harvest dates
					
					// special case (first month)
					if(m == field.plantingDate.month)
					{
						if(d >= field.plantingDate.day)
						{
							GDUs += wheatGduFunction(minTemp, maxTemp, GDUs);
						}
					}
					// special case (last month)
					else if(m == field.harvestDate.month)
					{
						if(d <= field.plantingDate.day)
						{
							GDUs += wheatGduFunction(minTemp, maxTemp, GDUs);
						}
					}
					// normal case
					else if(m > field.plantingDate.month && m < field.harvestDate.month)
					{
						GDUs += wheatGduFunction(minTemp, maxTemp, GDUs);
					}
				}
			}
			
			// return the total number of GDUs
			return GDUs;
		}
		else
		{
			// use Corn's GDD function
			
			
			// go through every day and add up the GDUs
			for(int m = 0; m < weatherData.tempHigh.length; m++)
			{
				for(int d = 0; d < weatherData.tempHigh[m].length; d++)
				{
					// skip ahead if we can
					if(m < field.plantingDate.month)
						m = field.plantingDate.month;
					
					
					// get the highs and lows for this day
					maxTemp = weatherData.tempHigh[m][d];
					minTemp = weatherData.tempLow[m][d];
					
					// only add GDU data between the planting and harvest dates
					
					// special case (first month)
					if(m == field.plantingDate.month)
					{
						if(d >= field.plantingDate.day)
						{
							GDUs += cornGduFunction(minTemp, maxTemp);
						}
					}
					// special case (last month)
					else if(m == field.harvestDate.month)
					{
						if(d <= field.plantingDate.day)
						{
							GDUs += cornGduFunction(minTemp, maxTemp);
						}
					}
					// normal case
					else if(m > field.plantingDate.month && m < field.harvestDate.month)
					{
						GDUs += cornGduFunction(minTemp, maxTemp);
					}
				}
			}
			
			// return the total number of GDUs
			return GDUs;
		}
	}
	
	/**
	 * This method gets the number of GDUs for the given day's temperature and
	 * how far the plant had grown thus far (the previousGDUs).
	 * @param minTemp
	 * @param maxTemp
	 * @param previousGDUs
	 */
	private static int cornGduFunction(double minTemp, double maxTemp)
	{
		/*
		 * If daily Max Temp > 86 �F (30 �C) it's set equal to 86�F (30 �C).
		 * If daily Max or Min Temp < 50 �F (10 �C), it's set equal to 50�F (10�C).
		 *  
		 *  Source: https://ndawn.ndsu.nodak.edu/help-corn-growing-degree-days.html
		 */
		
		
		// make sure that the maxTemp is within the correct bounds
		if(maxTemp < 50)
		{
			maxTemp = 50;
		}
		else if(maxTemp > 86)
		{
			maxTemp = 86;
		}
		
		// make sure that the minTemp is within the correct bounds
		if(minTemp < 50)
		{
			minTemp = 50;
		}
		else if(minTemp > 86)
		{
			maxTemp = 86;
		}
		
		// run the GDD function and add it to our total
		return (int) Math.floor(((maxTemp + minTemp)/2) - 50);
	}
	
	/**
	 * This method gets the number of GDUs for the given day's temperature and
	 * how far the plant had grown thus far (the previousGDUs). 
	 * @param minTemp
	 * @param maxTemp
	 * @param previousGDUs
	 */
	private static int wheatGduFunction(double minTemp, double maxTemp, int previousGDUs)
	{
		/*
		 * If daily Max or Min Temp < 32 �F (0 �C) it's set equal to 32 �F (0 �C). 
		 * Prior to Haun growth stage 2.0 or the accumulation of 395 GDD 
		 * from the planting date; If daily Max Temp > 70 �F (21 �C) 
		 * then it's set equal to 70 �F (21 �C). After Haun stage 2.0; 
		 * If daily Max Temp > 95 �F (35 �C ) it's set equal to 95 �F (35 �C).
		 *  
		 *  Source: https://www.ndawn.ndsu.nodak.edu/help-wheat-growing-degree-days.html
		 */
		
		// make sure that the maxTemp is within the correct bounds
		if(maxTemp < 32)
		{
			maxTemp = 32;
		}
		else if(maxTemp > 70 && previousGDUs < 395)
		{
			maxTemp = 70;
		}
		else if(maxTemp > 95)
		{
			maxTemp = 95;
		}
		
		// make sure that the minTemp is within the correct bounds
		if(minTemp < 32)
		{
			minTemp = 32;
		}
		else if(minTemp > 70 && previousGDUs < 395)
		{
			minTemp = 70;
		}
		else if(minTemp > 95)
		{
			minTemp = 95;
		}
		
		// run the GDD function and add it to our total
		return (int) Math.floor(((maxTemp + minTemp)/2) - 32);
	}
}
