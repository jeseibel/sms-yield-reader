package interpreters;


import java.util.ArrayList;

import objects.farm.FarmYear;
import objects.understood.UnderstoodLand;

/**
 * This class is in charge of creating UnderstoodLand objects.
 * 
 * @author James_Seibel
 * @version 06/04/2020
 */
public class LandInterpreter 
{
	
	/**
	 * Take in a farm year and output an arrayList containing
	 * UnderstoodLand objects for each field.
	 * @param fy the farm year to interpret
	 * @return an arrayList of understood objects
	 * @throws Exception if the same field had two different crops the same year
	 */
	public static ArrayList<UnderstoodLand> interpreteYear(FarmYear fy) throws Exception
	{
		// create an arraylist of Understood objects (one for each crop)
		ArrayList<UnderstoodLand> results = new ArrayList<UnderstoodLand>();
		
		// this is the name of the land we are dealing with
		String landName;
		
		// does a understood object exist for this land?
		boolean landExists = false;
		
		// go through every piece of field in this year
		for(int i = 0; i < fy.getNumbFields(); i++)
		{
			// we don't know if the required Understood objects exist yet
			landExists = false;
			
			// set the name of the land
			landName = fy.getField(i).name;

			// create a new Understood object if none exist
			if(results.size() == 0)
			{
				// no Understood object existed, create one for this field
				results.add(new UnderstoodLand());
				results.get(0).name = landName;
			}
			
			// see if there is an Understood object for this field
			for(UnderstoodLand u:results)
			{
				if(u.name.equalsIgnoreCase(landName))
				{
					// we found one break out of the loop
					landExists = true;
					break;
				}
			}			
			
			
			// did we find an Understood object for this field?
			if(landExists)
			{
				// we did, do nothing
			}
			else
			{
				// we didn't, add a new Understood object
				
				results.add(new UnderstoodLand());
				results.get(results.size()-1).name = landName;
			}
			
			
			
			// find which Understood object we needs to interacted with
			// so we can start adding data
			for(UnderstoodLand u:results)
			{
				// check if this Understood object is for the same piece of land
				if(u.name.equalsIgnoreCase(landName))
				{
					// we found the correct understood object
					// add relative information
					
					// set the name of the crop grown
					// if we try to overwrite a previous crop name, throw an exception
					// (since a piece of land shouldn't have more than one crop grown at a time)
					if(u.crop != null)
					{
						u.crop = fy.getField(i).crop;
					}
					else
					{
						throw new Exception(fy.getField(i).name + " held more than one crop in " + fy.year);
					}
					
					// find the rain for this field and add it to the running average
					// but only say that we found another piece of land if the rain is not zero
					if(fy.getField(i).totalRain != 0)
					{
						u.rain += fy.getField(i).totalRain;
					}
					
					// find the yield for this field and add it to the running average
					// but only say that we found another piece of land if there is yield data
					if(fy.getField(i).avgYield != 0)
					{
						u.avgYield += fy.getField(i).avgYield;
					}
					
					// find the GDUs for this field and add them to the running average
					// but only say that we found another piece of land if the GDU's is not zero
					if(GDU.getGDUs(
							fy.getField(i),
							fy.weatherData
							) != 0)
					{
						u.GDUs += GDU.getGDUs(fy.getField(i),fy.weatherData);
					}
					
					// get the year
					u.setYear(fy.year);
					
					// set the area of the field (in feet)
					u.areaFt = fy.getField(i).areaFt;
					
					// convert feet to acres
					u.areaAcres = fy.getField(i).areaFt / 43560;
					
					// using the area get the total yield for this field
					u.totalYield = u.areaAcres * u.avgYield;
					
					
					// update the harvest and planting dates
					
					// planting start
					if((u.getPlantingStartDate().day > fy.getField(i).plantingDate.day && 
							u.getPlantingStartDate().month >= fy.getField(i).plantingDate.month) 
							|| u.getPlantingStartDate().month > fy.getField(i).plantingDate.month)
					{
						u.getPlantingStartDate().day = fy.getField(i).plantingDate.day;
						u.getPlantingStartDate().month = fy.getField(i).plantingDate.month;
					}
					
					// planting end
					if((u.getPlantingStartDate().day < fy.getField(i).plantingDate.day && 
							u.getPlantingStartDate().month <= fy.getField(i).plantingDate.month) 
							|| u.getPlantingStartDate().month < fy.getField(i).plantingDate.month)
					{
						u.getPlantingStartDate().day = fy.getField(i).plantingDate.day;
						u.getPlantingStartDate().month = fy.getField(i).plantingDate.month;
					}
					
					// harvest start
					if((u.getPlantingStartDate().day > fy.getField(i).harvestDate.day && 
							u.getPlantingStartDate().month >= fy.getField(i).harvestDate.month) 
							|| u.getPlantingStartDate().month > fy.getField(i).harvestDate.month)
					{
						u.getPlantingStartDate().day = fy.getField(i).harvestDate.day;
						u.getPlantingStartDate().month = fy.getField(i).harvestDate.month;
					}
					
					// harvest end
					if((u.getPlantingStartDate().day < fy.getField(i).harvestDate.day && 
							u.getPlantingStartDate().month <= fy.getField(i).harvestDate.month) 
							|| u.getPlantingStartDate().month < fy.getField(i).harvestDate.month)
					{
						u.getPlantingStartDate().day = fy.getField(i).harvestDate.day;
						u.getPlantingStartDate().month = fy.getField(i).harvestDate.month;
					}
					
				}
				// wrong Understood object, keep looking
			}
		}
		
		// unlike the CropInterpreter we don't have to average anything.
		// Since each piece of field should only have 1 object 
		// in the farmYear anyway
		
		return results;
	}
	
	
}
