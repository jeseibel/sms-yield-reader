package objects;

/**
 * This object holds a month day and year.
 * This object doesn't do any checking for valid dates!
 * 
 * @author James_Seibel
 * @version 06-03-2020
 */
public class SimpleDate implements Comparable<SimpleDate>
{
	public int month;
	
	public int day;
	
	public int year;
	
	/**
	 * constructor
	 * 
	 * @param newMonth
	 * @param newDay
	 * @param newYear
	 */
	public SimpleDate(int newMonth, int newDay, int newYear)
	{
		month = newMonth;
		day = newDay;
		year = newYear;
	}
	
	/**
	 * default constructor
	 * 
	 * sets the date to -1/-1/-1
	 */
	public SimpleDate()
	{
		month = -1;
		day = -1;
		year = -1;
	}
	
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null)
			return false;
		
		if(obj.getClass().equals(SimpleDate.class))
		{
			SimpleDate compDate = (SimpleDate) obj;
			
			if(year != compDate.year)
				return false;
			if(month != compDate.month)
				return false;
			if(day != compDate.day)
				return false;
		}
		
		return true;
	}
	
	/**
	 * returns true if the year is after 1000,
	 * the month is between 0 and 11, and the
	 * day is between 0 and 30.
	 * 
	 * @return if the date is valid.
	 */
	public boolean isValidDate()
	{
		if(year < 1000)
			return false;
		if(month < 0 || month > 11)
			return false;
		if(day < 0 || day > 30)
			return false;
		
		return true;
	}
	
	
	@Override
	public String toString()
	{
		return month + "/" + day + "/" + year;
	}

	/**
	 * 1 = this date is more recent;
	 * -1 = this date is older
	 */
	@Override
	public int compareTo(SimpleDate other)
	{
		if(this.year > other.year)
		{
			return 1;
		}
		else if(this.year < other.year)
		{
			return -1;
		}
		else
		{
			if(this.month > other.month)
			{
				return 1;
			}
			else if(this.month < other.month)
			{
				return -1;
			}
			else
			{
				if(this.day > other.day)
				{
					return 1;
				}
				else if(this.day < other.day)
				{
					return -1;
				}
				else
				{
					// these two dates are the same
					return 0;
				}
					
			}
		}
	}
}
