package objects;

import enums.chemicalRateUnits;

/**
 * 
 * @author James_Seibel
 * @version 06-01-2020
 */
public class Chemical
{
	/** name of this chemical */
	public String name;
	
	/** how much of this chemical was applied */
	public Double amount;
	
	/** the units of applied chemical */
	public chemicalRateUnits rateUnits;
	
	
	/**
	 * default constructor
	 */
	public Chemical()
	{
		name = "unkown";
		amount = -1.0;
		rateUnits = chemicalRateUnits.UNKOWN;
	}
}
