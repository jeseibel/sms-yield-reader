package objects;


/**
 * This object holds all data related to the weather for a given year
 * @author James_Seibel
 * @version 12-26-2019
 */
public class Weather 
{
	/** this is the year that this weather object is associated with */
	public int year;
	
	/** This is the amount of rain that fell on a specific day in inches,
	 * between March 1st and November 30th.
	 * The first dimension is the month, and the second is the day  */
	public double[][] rain; //274 days total
	
	/** This is the temperature high (in Fahrenheit) on any given day,
	 * between March 1st and November 30th.
	 * The first dimension is the month, and the second is the day  */
	public double[][] tempHigh; //274 days total
	
	/** This is the temperature low (in Fahrenheit) on any given day,
	 * between March 1st and November 30th.
	 * The first dimension is the month, and the second is the day  */
	public double[][] tempLow; //274 days total
	
	
	
	/**
	 * default constructor
	 */
	public Weather()
	{
		// do nothing
	}
	
}
