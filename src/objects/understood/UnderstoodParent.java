package objects.understood;

import enums.understoodType;
import objects.SimpleDate;

/**
 * This is the parent to all understood object and
 * holds all shared methods and variables.
 * 
 * @author James_Seibel
 * @version 04-12-2020
 */
public class UnderstoodParent implements UnderstoodInterface
{
	/** This is the crop this object represents */
	public String name;
	
	/** this is the year this object takes data from */
	protected int year;
	
	/** this is the type of understood object this is */
	protected understoodType type;
	
	
	
	/** this is the date that planting started */
	protected SimpleDate plantingStartDate;
	
	/** this is the date planting ended */
	protected SimpleDate plantingEndDate;
	
	
	/** this is the date harvest started */
	protected SimpleDate harvestStartDate;
	
	/** this is the date harvest ended */
	protected SimpleDate harvestEndDate;
	
	
	
	
	/**
	 * default constructor
	 */
	public UnderstoodParent()
	{
		// set the default variables
		name = null;
		year = -1;
		
		plantingStartDate = new SimpleDate(Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE);
		plantingEndDate = new SimpleDate(0,0,-1);

		harvestStartDate = new SimpleDate(Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE);
		harvestEndDate = new SimpleDate(0,0,-1);
	}
	
	
	@Override
	public void truncateData(int sigFigs)
	{
		// Understood Parent has nothing to truncate
		System.err.println("ERROR: truncateData method not implemented");
	}
	
	/**
	 * set the harvest start date.
	 * @param month
	 * @param day
	 * @param year
	 */
	@Override
	public void setHarvestStartDate(int month, int day)
	{
		harvestStartDate.month = month;
		harvestStartDate.day = day;
		harvestStartDate.year = year;
	}
	
	/**
	 * get the harvest start date.
	 * @return
	 */
	@Override
	public SimpleDate getHarvestStartDate()
	{
		return harvestStartDate;
	}
	
	/**
	 * set the harvest end date.
	 * @param month
	 * @param day
	 * @param year
	 */
	@Override
	public void setHarvestEndDate(int month, int day)
	{
		harvestEndDate.month = month;
		harvestEndDate.day = day;
		harvestEndDate.year = year;
	}
	
	/**
	 * get the harvest end date
	 * @return
	 */
	@Override
	public SimpleDate getHarvestEndDate()
	{
		return harvestEndDate;
	}
	
	
	/**
	 * set the planting start date.
	 * @param month
	 * @param day
	 * @param year
	 */
	@Override
	public void setPlantingStartDate(int month, int day)
	{
		plantingStartDate.month = month;
		plantingStartDate.day = day;
		plantingStartDate.year = year;
	}
	
	/**
	 * get the planting start date.
	 * @return
	 */
	@Override
	public SimpleDate getPlantingStartDate()
	{
		return plantingStartDate;
	}
	
	/**
	 * set the planting end date.
	 * @param month
	 * @param day
	 * @param year
	 */
	@Override
	public void setPlantingEndDate(int month, int day)
	{
		plantingEndDate.month = month;
		plantingEndDate.day = day;
		plantingEndDate.year = year;
	}
	
	/**
	 * get the planting end date
	 * @return
	 */
	@Override
	public SimpleDate getPlantingEndDate()
	{
		return plantingEndDate;
	}

	/**
	 * silently reject negative years.
	 * @param newYear
	 */
	@Override
	public void setYear(int newYear)
	{
		if(newYear < -1)
			return;
		
		year = newYear;
		plantingStartDate.year = newYear;
		plantingEndDate.year = newYear;
		harvestStartDate.year = newYear;
		harvestEndDate.year = newYear;
	}

	@Override
	public int getYear()
	{
		return year;
	}
	
	
	@Override
	public understoodType getType()
	{
		return type;
	}

	@Override
	public int compareTo(UnderstoodInterface u)
	{
		return ((UnderstoodParent) u).name.compareToIgnoreCase(name);
	}
}
