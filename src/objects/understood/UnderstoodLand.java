package objects.understood;

import enums.understoodType;

/**
 * This object is created and interacted with by the Interpreter.
 * It holds any information that is going to be shown.
 * @author James_Seibel
 * @version 12-26-2019
 */
public class UnderstoodLand extends UnderstoodParent
{
	/** this is the crop this field held */
	public String crop;
	
	/** this is the average yield for this field */
	public double avgYield;
	
	/** this is the total yield for this field */
	public double totalYield;
	
	/** Growth Development Units. AKA Growth Development Days */
	public int GDUs;
	
	/** this is the total amount of rain that this field received */
	public double rain;
	
	/** This is the average moisture of the grain at harvest */
	public double avgMoisture;
	
	
	/** this is the area of the harvested field in Feet */
	public double areaFt;
	
	/** this is the area of the harvested field in Acres */
	public double areaAcres;
	
	
	/**
	 * default constructor
	 */
	public UnderstoodLand()
	{
		// call the UnderstoodParent constructor
		super();
		
		
		// set the default variables
		crop = "";
		avgYield = 0;
		totalYield = 0;
		GDUs = 0;
		
		areaFt = 0.0;
		areaAcres = 0.0;
		
		rain = 0;
		avgMoisture = 0;
	}
	
	@Override
	public String toString()
	{
		String returnString = "";
		
		returnString = "land: " + name + "\n";
		
		returnString += "yield: " + avgYield + " Bu/ac \n";
		returnString += "avg GDUs: " + GDUs + "\n";
		
		returnString += "avg Rain: " + rain + " in. \n";
		returnString += "avg Moisture: " + avgMoisture + " % \n";
		
		returnString += "Planting date: " + plantingStartDate.toString() + "\n";
		returnString += "Harvest date: " + harvestStartDate.toString() + "\n";
		
		
		return returnString;
	}
	
	/**
	 * this function will truncate all doubles to the given
	 * number of sigFigs, in this object.
	 * With <= 0 being no truncation, and the max being 9.
	 * @param sigFigs the number of significant figures to keep
	 */
	@Override
	public void truncateData(int sigFigs)
	{
		// don't do any truncation
		if(sigFigs <= 0)
			return;
		
		// only allow a max of 9 sigFigs
		if(sigFigs > 9)
			sigFigs = 9;
		
		int truncFactor = 1;
		
		// set how much we want to truncate by
		for(int i = 0; i < sigFigs; i++)
		{
			truncFactor *= 10;
		}
		
		// truncate the doubles we have stored
		avgYield = Math.floor(avgYield * truncFactor) / truncFactor;
		totalYield = Math.floor(totalYield * truncFactor) / truncFactor;
		rain = Math.floor(rain * truncFactor) / truncFactor;
		avgMoisture = Math.floor(avgMoisture * truncFactor) / truncFactor;
		areaFt = Math.floor(areaFt * truncFactor) / truncFactor;
		areaAcres = Math.floor(areaAcres * truncFactor) / truncFactor;
	}
		
	
	@Override
	public int compareTo(UnderstoodInterface u) 
	{
		if(u.getType() == understoodType.LAND)
			// only compare based on the land name
			return ((UnderstoodLand) u).name.compareToIgnoreCase(name);
		else
			return -1;
	}

	
}
