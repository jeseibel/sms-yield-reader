package objects.understood;

import enums.understoodType;
import objects.SimpleDate;

/**
 * This is the parent of understood objects, these objects hold
 * interpreted data.
 * 
 * @author James_Seibel
 * @version 04-12-2020
 */
public interface UnderstoodInterface extends Comparable<UnderstoodInterface>
{
	/**
	 * This returns what type of understood object this is.
	 * @return what understood object this is.
	 */
	public understoodType getType();
	
	/**
	 * this function will truncate all doubles to the given
	 * number of sigFigs, in this object.
	 * With <= 0 being no truncation, and the max being 9.
	 * @param sigFigs the number of significant figures to keep
	 */
	public abstract void truncateData(int sigFigs);
	

	
	
	
	
	
	public void setHarvestStartDate(int month, int day);
	
	public SimpleDate getHarvestStartDate();
	
	
	public void setHarvestEndDate(int month, int day);
	
	public SimpleDate getHarvestEndDate();
	
	
	
	
	public void setPlantingStartDate(int month, int day);
	
	public SimpleDate getPlantingStartDate();
	
	
	public void setPlantingEndDate(int month, int day);
	
	public SimpleDate getPlantingEndDate();
	
	
	
	public void setYear(int newYear);
	
	public int getYear();
}
