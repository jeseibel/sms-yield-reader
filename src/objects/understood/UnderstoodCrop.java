package objects.understood;

import enums.understoodType;

/**
 * This object is created and interacted with by the Interpreter.
 * It holds any information that is going to be shown.
 * @author James_Seibel
 * @version 04-12-2020
 */
public class UnderstoodCrop extends UnderstoodParent
{	
	/** this is the average yield (in bu/ac) for this crop during this year */
	public double avgYield;
	
	/** this is the number of bushels that this crop produced */
	public double totalYield;
	
	/** Growth Development Units. AKA Growth Development Days */
	public int avgGDUs;
	
	/** this is the average rainfall that this land received */
	public double avgRain;
	
	/** This is the average moisture of the grain at harvest */
	public double avgMoisture;
	
	/** this is the total area this crop was harvested in acres */
	public double totalAreaAcres;
	
	/** this is the total area this crop was harvested in feet */
	public double totalAreaFt;
	
	
	
	/**
	 * default constructor
	 */
	public UnderstoodCrop()
	{
		// call the UnderstoodParent constructor
		super();
		
		
		// set the default variables
		avgYield = 0;
		totalYield = 0;
		avgGDUs = 0;
		
		avgRain = 0;
		avgMoisture = 0;
		
		totalAreaAcres = 0;
		totalAreaFt = 0;
		
		type = understoodType.CROP;
	}
	
	/**
	 * this function will truncate all doubles to the given
	 * number of sigFigs, in this object.
	 * With <= 0 being no truncation, and the max being 9.
	 * @param sigFigs the number of significant figures to keep
	 */
	@Override
	public void truncateData(int sigFigs)
	{
		// don't do any truncation
		if(sigFigs <= 0)
			return;
		
		// only allow a max of 9 sigFigs
		if(sigFigs > 9)
			sigFigs = 9;
		
		int truncFactor = 1;
		
		// set how much we want to truncate by
		for(int i = 0; i < sigFigs; i++)
		{
			truncFactor *= 10;
		}
		
		// truncate the doubles we have stored
		avgYield = Math.floor(avgYield * truncFactor) / truncFactor;
		avgRain = Math.floor(avgRain * truncFactor) / truncFactor;
		avgMoisture = Math.floor(avgMoisture * truncFactor) / truncFactor;
		totalYield = Math.floor(totalYield * truncFactor) / truncFactor;
		totalAreaAcres = Math.floor(totalAreaAcres * truncFactor) / truncFactor;
		totalAreaFt = Math.floor(totalAreaFt * truncFactor) / truncFactor;
	}

	
	@Override
	public String toString()
	{
		String returnString = "";
		
		returnString = "crop: " + name + "\n";
		
		returnString += "average yield: " + avgYield + " Bu/ac \n";
		returnString += "total yield: " + totalYield + " Bu. \n";
		returnString += "avg GDUs: " + avgGDUs + "\n";
		
		returnString += "avg Rain: " + avgRain + " in. \n";
		returnString += "avg Moisture: " + avgMoisture + " % \n";
		
		returnString += "area: " + totalAreaAcres + " ac \n";
		
		returnString += "Planting date: " + plantingStartDate.toString() + "\n";
		returnString += "Harvest date: " + harvestStartDate.toString();
		
		
		return returnString;
	}
	
	@Override
	public int compareTo(UnderstoodInterface u) 
	{
		if(u.getType() == understoodType.CROP)
			// only compare based on the land name
			return ((UnderstoodCrop) u).name.compareToIgnoreCase(name);
		else
			return -1;
	}
}
