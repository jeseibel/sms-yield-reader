package objects.parsing;

import enums.chemicalRateUnits;
import objects.SimpleDate;

/**
 * This object is used in the SMS Parser to temporarily store data
 * while reading in the file.
 * 
 * 
 * @author James_Seibel
 * @version 06-01-2020
 */
public class PendingObject
{
	/** This is the name of the pending field. */
	public String name;
	
	/** This is the name of the crop in the field. */
	public String crop;
	
	/** This is the name of the chemical in this field */
	public String chemical;
	
	
	
	/** this stores the date listed in the file */
	public SimpleDate date;
	
	
	
	// pending variables need to be divided by the number of lines
	// in order to get their actual values
	
	/** This is used when initially reading the file so we can determine the
	 * average yield later. This should not be saved or used in any way
	 * after the file has been parsed! */
	public double pendingYield;
	
	/** This is used when initially reading the file so we can determine the
	 * average yield later. This should not be saved or used in any way
	 * after the file has been parsed! */
	public double pendingMoisture;
	
	/** This is used when initially reading the file so we can determine the
	 * average Implement Width. This should not be saved or used in any way
	 * after the file has been parsed! */
	public double pendingImplementWidth;

	
	/** This is used when initially reading the file so we can determine the
	 * average Chemical amount. This should not be saved or used in any way
	 * after the file has been parsed! */
	public double pendingChemical;
	
	/** the units of applied chemical */
	public chemicalRateUnits chemicalRateUnits;
	
	
	/** this is the total distance the implement traveled,
	 * used with implement width to determine the area of the field */
	public double distance;
	
	
	/**
	 * default constructor
	 */
	public PendingObject()
	{
		name = "unkown";
		crop = "unkown";
		date = new SimpleDate();
		pendingYield = 0;
		pendingMoisture = 0;
		pendingChemical = 0;
		pendingImplementWidth = 0;
		distance = 0;
	}
}
