package objects.farm;

import java.util.ArrayList;

import objects.Chemical;
import objects.SimpleDate;

/**
 * 
 * @author James_Seibel
 * @version 06-03-2020
 */
public class Field 
{
	/** this is the name of this field */
	public String name;
	
	/** this is the name of the crop grown on this land */
	//public String crop;
	
	/** this is the name of the crop type grown on this land.
	 * IE: corn or soybeans */
	public String crop;
	
	/** this is the average yield on this land in bu/acre */
	public double avgYield;
	
	/** this is the average moisture at harvest */
	public double avgMoisture;
	
	/** this is the amount of rain that this piece of land got,
	 * between planting and harvest */
	public double totalRain;
	
	/** this is the area of this land in feet */
	public double areaFt;
	
	/** this is the are of this land in acres */
	public double areaAcres;
	
	/** this stores every chemical application that this field received */
	public ArrayList<ChemicalApplication> chemicalApplications;
	
	
	
	/** year this field was planted and harvested. */
	public int year;
	
	/** this is the date this land was planted.
	 *  (if it was planted over multiple days, this will show the day it was started) */
	public SimpleDate plantingDate;
	
	/** this is the day this land was harvested.
	 *  (if it was harvested over multiple days, this will show the day it was started) */
	public SimpleDate harvestDate;
	
	
	/**
	 * default constructor
	 */
	public Field()
	{
		name = "N/A";
		crop = "Unkown";
		avgYield = 0;
		avgMoisture = 0;
		totalRain = 0;
		areaFt = 0;
		areaAcres = 0;
		chemicalApplications = new ArrayList<>();
		year = -1;
		plantingDate = new SimpleDate();
		harvestDate = new SimpleDate();
	}
	
	
	
	/**
	 * This function merges the given field into this one.
	 * NOTE: This assumes that the 2 pieces of land are the same! Bugs will happen
	 * if you try to merge two pieces of land that aren't the same.
	 * @param oldLand 
	 * @param newField
	 * @throws IllegalArgumentException if the field passed in doesn't have the same name or year
	 * @return the old Field with updated values
	 */
	public void merge(Field newField) throws IllegalArgumentException
	{
		// don't allow merging fields that have different names
		if(!name.equals(newField.name))
			throw new IllegalArgumentException("Tried to merge fields with different names: " + newField.name +
					" into " + name);
		
		// don't allow merging fields that have different years
		if(year > 0 && newField.year > 0 && year != newField.year)
			throw new IllegalArgumentException("Tried to merge fields with different years: " + newField.year +
					" into " + year);
		
		
		// add the area of the Fields together
		// (we are assuming that if there was multiple pieces of land
		// they were from the same harvest and should add up to the
		// total amount of land)
		areaFt += newField.areaFt;
		areaAcres += newField.areaAcres;
		
		
		
		///////////
		// yield //
		///////////
		
		// check to see if the oldLand has a yield
		if(avgYield == 0)
		{
			// the oldLand has no yield
			// replace it with the newLand
			avgYield = newField.avgYield;
		}
		else
		{
			// the oldLand has a yield
			
			// does the newLand?
			if(newField.avgYield != 0)
			{
				// the old and new land both have yields
				// find the average and put it into the oldLand
				avgYield += newField.avgYield;
				avgYield /= 2;
			}
			// the new land doesn't have a yield, do nothing
		}
		
		
		//////////////
		// moisture //
		//////////////
		
		// check to see if the oldLand has a moisture
		if(avgMoisture == 0)
		{
			// the oldLand has no moisture
			// replace it with the newLand
			avgMoisture = newField.avgMoisture;
		}
		else
		{
			// the oldLand has a moisture

			// does the newLand?
			if(newField.avgMoisture != 0)
			{
				// the old and new land both have moisture
				// find the average and put it into the oldLand

				avgMoisture += newField.avgMoisture;
				avgMoisture /= 2;

			}
			// the new land doesn't have a moisture, do nothing
		}

		
		//////////
		// rain //
		//////////
		
		// check to see if the oldLand has any rain
		if(totalRain == 0)
		{
			// the oldLand has no rain
			// replace it with the newLand
			totalRain = newField.totalRain;
		}
		
		
		
		
		
		///////////////////////////
		// chemical applications //
		///////////////////////////
		
		// only overwrite or add the old land's chemicals if
		// the new land has chemicals
		mergeChemApps(newField.chemicalApplications);
		
		
		
		
		///////////
		// dates //
		///////////
		
		// planting date, go with the older one
		
		// only continue if newField has a valid planting date
		if(newField.plantingDate.isValidDate())
		{
			// only continue if this field's planting date isn't valid, or
			// newField's planting date is after this field's
			if(!plantingDate.isValidDate() || plantingDate.compareTo(newField.plantingDate) == 1)
			{
				plantingDate = newField.plantingDate;
			}
		}
		
		
		// harvest date, go with the more recent one
		
		// only continue if newField has a valid harvest date
		if(newField.harvestDate.isValidDate())
		{
			// only continue if this field's harvest date isn't valid, or
			// newField's planting date is after this field's or null
			if(!harvestDate.isValidDate() || harvestDate.compareTo(newField.harvestDate) == 1)
			{
				harvestDate = newField.harvestDate;
			}
		}
	}
	
	/**
	 * Merge the given chemical applications into this field.
	 * NOTE: this doesn't merge chemicals, they just be added separately.
	 * 
	 * @param chemApps
	 */
	private void mergeChemApps(ArrayList<ChemicalApplication> chemApps)
	{
		for(ChemicalApplication oldChemApp : chemicalApplications)
		{
			for(ChemicalApplication newChemApp : chemApps)
			{
				// go through each old chemical application and
				// try to merge with them with the new Chemical Application
				
				// only merge the applications if they were applied the same day
				if(oldChemApp.applicationDate.equals(newChemApp.applicationDate))
				{
					// add the chemicals from the old chemical application
					
					for(Chemical newChemical : newChemApp.chemicals)
					{
						oldChemApp.chemicals.add(newChemical);
					}
				}
			}
		}
	}
	
	/**
	 * this function will truncate all doubles to the given
	 * number of sigFigs.
	 * With <= 0 being no truncation, and the max being 9.
	 * 
	 * @param sigFigs the number of significant figures to keep
	 */
	public void truncateData(int sigFigs)
	{
		// don't do any truncation
		if(sigFigs <= 0)
			return;
		
		// only allow a max of 9 sigFigs
		if(sigFigs > 9)
			sigFigs = 9;
		
		int truncFactor = 1;
		
		// set how much we want to truncate by
		for(int i = 0; i < sigFigs; i++)
		{
			truncFactor *= 10;
		}
		
		// truncate the doubles we have stored
		avgMoisture = Math.floor(avgMoisture * truncFactor) / truncFactor;
		avgYield = Math.floor(avgYield * truncFactor) / truncFactor;
		areaAcres = Math.floor(areaAcres * truncFactor) / truncFactor;
		areaFt = Math.floor(areaFt * truncFactor) / truncFactor;
	}
	
	@Override
	public String toString()
	{
		String data = name + "\n" 
					+ crop +"\n" 
					+ avgYield + " bu/acre\n" 
					+ "total rain: " + totalRain + "in. " + "\n"
					+ "Harvest moisture: " + avgMoisture + " %" + "\n"
					+ areaFt + " sq ft" + "\n"
					+ areaAcres + " Acres" + "\n"
					+ "Planting date: " + plantingDate.toString() + "\n"
					+ "Harvest date: " + harvestDate.toString();
		
		return data;
		
	}
}
