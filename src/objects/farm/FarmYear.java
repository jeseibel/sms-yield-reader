package objects.farm;


import java.util.ArrayList;

import objects.Weather;

/**
 * This custom DataType holds all that data relevant to our program
 * for a given year.
 * 
 * @author James_Seibel
 * @version 12-26-2019
 */
public class FarmYear
{
	/** This arrayList holds every piece of land that was farmed this year */
	private ArrayList<Field> fieldArray;
	
	/** This object holds all data related to the weather for this year */
	public Weather weatherData;
	
	/** This is the year this FarmYear represents */
	public int year;
	
	/**
	 * default constructor
	 */
	FarmYear()
	{
		fieldArray = new ArrayList<Field>();
		year = -1;
	}
	
	/**
	 * constructor that also sets the year
	 * @param year
	 */
	public FarmYear(int newYear)
	{
		fieldArray = new ArrayList<Field>();
		year = newYear;
	}
	
	
	/**
	 * This function tries to add a new piece of Field to the ArrayList.
	 * If a piece of land with the same name already exits the two are merged
	 * @param newLand the Field that we are trying to add
	 */
	public void addOrMergeField(Field newLand)
	{
		// this holds the index of the first duplicate found
		// (or -1 if there is no duplicate)
		int dupIndex = indexOfDuplicate(newLand);
		
		// check if there is already a Field
		// with the same name
		if(dupIndex == -1)
		{
			// there are no duplicates
			// add the newLand normally
			
			fieldArray.add(newLand);
		}
		else
		{
			// there is a duplicate!
			
			// merge the two files together
			fieldArray.get(dupIndex).merge(newLand);
		}
		
	}
	
	/**
	 * This function looks through the FieldArray for any
	 * pieces of land that have the same name as "newLand". 
	 * The first one it comes across with the same name is 
	 * the index it will return
	 * @param newLand
	 * @return the index of the first piece of land with the same name as newLand
	 */
	public int indexOfDuplicate(Field newLand)
	{
		int loop = 0;
		
		// go through the Field array
		for(Field c:fieldArray)
		{
			// check if these two pieces of land have the same name
			if(c.name.equals(newLand.name))
			{
				// they have the same name!
				// return the index of the duplicate
				return loop;
			}
			
			// they didn't have the same name,
			// increment the loop and keep looking
			loop++;
		}
		
		// we didn't find any duplicates
		return -1;
	}
	
	
	
	/**
	 * this returns how many fields are stored in the array
	 * @return the number of field
	 */
	public int getNumbFields()
	{
		return fieldArray.size();
	}
	
	/**
	 * This returns the field at the given index
	 * @param index
	 * @return the Field at the given index
	 */
	public Field getField(int index)
	{
		return fieldArray.get(index);
	}
	
	@Override
	public String toString()
	{
		// the new line char is so that it is easier to read a long
		// array of FarmYears in the debugger
		return year + ":\t" + fieldArray.size() + " fields\n";
	}
}
