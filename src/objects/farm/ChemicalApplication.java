package objects.farm;

import java.util.ArrayList;

import objects.Chemical;
import objects.SimpleDate;

/**
 * 
 * @author James_Seibel
 * @version 06-01-2020
 */
public class ChemicalApplication
{
	/** the chemicals that were applied in this application */
	public ArrayList<Chemical> chemicals;
	
	
	/** this is the date the chemicals were applied */
	public SimpleDate applicationDate;
	
	
	public ChemicalApplication()
	{
		chemicals = new ArrayList<>();
		
		applicationDate = new SimpleDate(0,0,-1);
	}
}
